<!DOCTYPE html>
<html>
<head>
	<title>Phương trình bậc nhất</title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: black;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Giải phương trình bậc nhất</h4>
		</div>
		<div id="form" style="padding-left: 50px;">
			<b>Giải phương trình bậc nhất</b><br><br>
			<form action="xlptb1.php" method="get">
			  <input type="text" id="so1" name="so1">
			  X +
			  <input type="text" id="so2" name="so2">
			  = 0<br><br>
			  <input type="submit" name="tinh" value="Tính">
			</form>
		</div>
	</div>
</body>
</html>