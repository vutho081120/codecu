<!DOCTYPE html>
<html>
<head>
	<title>Phương trình bậc nhất</title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: black;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<?php 
		$so1 = $_GET['so1'];
		$so2 = $_GET['so2'];
		if (empty($so1) || empty($so2)) {
			$kq = "Mời bạn nhập dữ liệu";
		}
		elseif (!is_numeric($so1) || !is_numeric($so2)) {
			$kq = "Dữ liệu nhập số sai kiểu";
		}
		else{
			if($so1 == 0) {
				if ($so2 == 0) {
					$kq = "Phương trình vô số nghiệm";
				}
				else{
					$kq = "Phương trình vô nghiệm";
				}
			}
			else{
				$kq = "Phương trình có nghiệm X = ".(-$so2/$so1);
			}
		}
	?>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Giải phương trình bậc nhất</h4>
		</div>
		<div id="form" style="padding-left: 50px;">
			<font size="6">
	  			<?php 
					echo $kq;
	  			?>
			</font>
		</div>
	</div>
</body>
</html>