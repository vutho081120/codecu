<!DOCTYPE html>
<html>
<head>
	<title>Tính tiền điện</title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
		#cscu{
			width: 300px;
			padding: 20px 0px 20px 20px;
			float: left;
		}
		#csmoi{
			width: 300px;
			padding: 20px 0px 20px 10px;
			float: left;
		}
		#khuvuc{
			width: 250px;
			padding: 20px 0px 20px 10px;
			float: left;
		}
		#tinh{
			clear: left;
			width: 900px;
			text-align: center;
		}
		#p1, #p2, #p3{
			margin: 0px 0px;
			font-size: x-large;
			display: inline-block;
		}
		#i1, #i2{
			width: 130px;
			border-color: blue;
		}
		#i3{
			width: 100px;
			border-radius: 6px;
			border-color: blue;
			background-image: linear-gradient(180deg, #DFE581, #95BBB0);
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">TÍNH TIỀN ĐIỆN</h4>
		</div>
		<form action="xltiendien.php" method="get">
			<div id="cscu">
				<p id="p1">Chỉ số cũ</p> <br>
				<input id="i1" type="text" name="cu">
			</div>
			<div id="csmoi">
				<p id="p2">Chỉ số mới</p> <br>
				<input id="i2" type="text" name="moi">
			</div>
			<div id="khuvuc">
				<p id="p3">Khu vực</p>
				<select name="kv">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
				</select>
			</div>
			<div id="tinh">
				<input id="i3" type="submit" name="submit" value="Tính">
			</div>
		</form>
	</div>
</body>
</html>