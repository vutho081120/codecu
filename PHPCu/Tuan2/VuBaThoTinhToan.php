<!DOCTYPE html>
<html>
<head>
	<title>Phép toán số học</title>
	<style type="text/css">
		#noidung{
			margin: 100px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: white;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<?php
		if (isset($_GET['xem'])) {
			$so1 = $_GET['sothunhat'];
			$pt = $_GET['pheptoan'];
			$so2 = $_GET['sothuhai'];
		}
		else {
			$so1 = "";
			$pt = "";
			$so2 = "";
		}
		
		if (empty($so1) || empty($pt) || empty($so2)) {
			$kq = "Mời bạn nhập dữ liệu";
		}
		elseif (!is_numeric($so1) || !is_numeric($so2)) {
			$kq = "Dữ liệu nhập số sai kiểu";
		}
		else {
			switch ($pt) {
				case '+':
					$kq = "Kết quả: ".$so1." + ".$so2." = ".($so1+$so2);
					break;
				case '-':
					$kq = "Kết quả: ".$so1." - ".$so2." = ".($so1-$so2);
					break;
				case '*':
					$kq = "Kết quả: ".$so1." * ".$so2." = ".($so1*$so2);
					break;
				case '/':
					$kq = "Kết quả: ".$so1." / ".$so2." = ".($so1/$so2);
					break;
				default:
					$kq = "Phép toán không đúng";
					break;
			}
		}
	?>
	<div id="noidung">
		<div id="tieude">
			<h2 style="display: inline-block;">Phép toán số học</h2>
		</div>
		<div id="form">
			<form action="VuBaThoTinhToan.php" method="get">
			  	<font size="5">
			  		<div style="float: left; width: 250px; padding-left: 50px;">
			  			<br>Số thứ nhất: <br><br>
			  			Phép toán: <br><br>
			  			Số thứ hai: <br><br>
			  		</div>
			  		<div style="float: left; width: 250px;">
			  			<br><input id="i1" type="text" name="sothunhat" value="<?php echo $so1 ?>"> <br><br>
			  			<input id="i2" type="text" name="pheptoan" value="<?php echo $pt ?>"> <br><br>
			  			<input id="i3" type="text" name="sothuhai" value="<?php echo $so2 ?>">
			  		</div>
				  	<div style="text-align: center; clear: left;">
				  		<input type="submit" value="Xem kết quả" name="xem"> <br><br>
				  	</div>
			  	</font>
			  	
			  	<div style="text-align: center;">
			  		<font size="6" style="">
			  			<?php 
							echo $kq;
			  			?>
			  		</font>
			  		<br><br>
			  	</div>
			</form>
		</div>
	</div>
</body>
</html>