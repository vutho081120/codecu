<!DOCTYPE html>
<html>
<head>
	<title>Thời Khóa Biểu</title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: white;
			text-align: center;
			background-color: blue;
		}
		#dngay{
			text-align: center;
			padding-top: 10px;
			padding-bottom: 5px;
		}
		#dtiet{
			text-align: center;
			padding-top: 10px;
			padding-bottom: 5px;
		}
	</style>
</head>
<body>
	<?php
		if (isset($_GET['xem'])) {
			$thu = $_GET['thu'];
			$tiet = $_GET['tiet'];
		}
		else {
			$thu = "2";
			$tiet = "1";
		}
		
		switch ($thu) {
			case "2":
				if($tiet == 1 || $tiet == 2 || $tiet == 3){
					$kq = "Mạng máy tính nâng cao";
				}
				elseif ($tiet == 6 || $tiet == 7) {
					$kq = "Phân tích thiết kế thuật toán";
				}
				else{
					$kq = "Nghỉ";
				}
				break;
			case "3":
				if($tiet == 1 || $tiet == 2 || $tiet == 3){
					$kq = "Thực hành kỹ thuật máy tính và mạng";
				}
				elseif ($tiet == 6 || $tiet == 7 || $tiet == 8) {
					$kq = "Trí tuệ nhân tạo";
				}
				else{
					$kq = "Nghỉ";
				}
				break;
			case "4":
				if($tiet == 6 || $tiet == 7){
					$kq = "Bài tập lớn";
				}
				elseif ($tiet == 8 || $tiet == 9 || $tiet == 10) {
					$kq = "Tiếng anh chuyên ngành 2";
				}
				else{
					$kq = "Nghỉ";
				}
				break;
			case "5":
				if($tiet == 1 || $tiet == 2 || $tiet == 3){
					$kq = "Lập trình mạng 2";
				}
				else{
					$kq = "Nghỉ";
				}
				break;
			case "6":
				if($tiet == 2 || $tiet == 3 || $tiet == 4 || $tiet == 5){
					$kq = "Phân tích thiết kế hệ thống";
				}
				else{
					$kq = "Nghỉ";
				}
				break;
			case "7":
				$kq = "Nghỉ";
				break;
			case "8":
				$kq = "Nghỉ";
				break;
			default:
				$kq = "Nghỉ";
				break;
		}
	?>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">THỜI KHÓA BIỂU CỦA SINH VIÊN</h4>
		</div>
		<div id="form">
			<form action="VuBaThoTKB2.php" method="get">
			  	<div id="dngay">
			  		<b>Ngày:</b>
				  	<select name="thu">
						<option value="2" <?php if(isset($_GET['thu']) && $thu == 2){
						echo "selected";
					} ?>>2</option>
						<option value="3" <?php if(isset($_GET['thu']) && $thu == 3){
						echo "selected";
					} ?>>3</option>
						<option value="4" <?php if(isset($_GET['thu']) && $thu == 4){
						echo "selected";
					} ?>>4</option>
						<option value="5" <?php if(isset($_GET['thu']) && $thu == 5){
						echo "selected";
					} ?>>5</option>
						<option value="6" <?php if(isset($_GET['thu']) && $thu == 6){
						echo "selected";
					} ?>>6</option>
						<option value="7" <?php if(isset($_GET['thu']) && $thu == 7){
						echo "selected";
					} ?>>7</option>
						<option value="8" <?php if(isset($_GET['thu']) && $thu == 8){
						echo "selected";
					} ?>>CN</option>
					</select>
			  	</div>
			  	<div id="dtiet">
			  		<b>Tiết:</b>
					<select name="tiet">
						<option value="1" <?php if(isset($_GET['tiet']) && $tiet == 1){
						echo "selected";
					} ?>>1</option>
						<option value="2" <?php if(isset($_GET['tiet']) && $tiet == 2){
						echo "selected";
					} ?>>2</option>
						<option value="3" <?php if(isset($_GET['tiet']) && $tiet == 3){
						echo "selected";
					} ?>>3</option>
						<option value="4" <?php if(isset($_GET['tiet']) && $tiet == 4){
						echo "selected";
					} ?>>4</option>
						<option value="5" <?php if(isset($_GET['tiet']) && $tiet == 5){
						echo "selected";
					} ?>>5</option>
						<option value="6" <?php if(isset($_GET['tiet']) && $tiet == 6){
						echo "selected";
					} ?>>6</option>
						<option value="7" <?php if(isset($_GET['tiet']) && $tiet == 7){
						echo "selected";
					} ?>>7</option>
						<option value="8" <?php if(isset($_GET['tiet']) && $tiet == 8){
						echo "selected";
					} ?>>8</option>
						<option value="9" <?php if(isset($_GET['tiet']) && $tiet == 9){
						echo "selected";
					} ?>>9</option>
						<option value="10" <?php if(isset($_GET['tiet']) && $tiet == 10){
						echo "selected";
					} ?>>10</option>
					</select>
			  	</div>
			  	<div style="text-align: center;">
			  		<input type="submit" value="Xem" name="xem"> <br><br>
			  		<input type="submit" value="Nhập lại" name="nhaplai">
			  	</div>
			  	<div style="text-align: center;">
			  		<font size="6" style="color: purple;">
			  			<?php 
			  				if (isset($_GET['xem'])) {
								echo $kq;
							}
			  			?>
			  		</font>
			  	</div>
			</form>
		</div>
	</div>
</body>
</html>