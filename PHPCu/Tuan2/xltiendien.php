<!DOCTYPE html>
<html>
<head>
	<title>Tính tiền điện</title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 300px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
		#cscu{
			width: 300px;
			padding: 20px 0px 20px 20px;
			float: left;
		}
		#csmoi{
			width: 300px;
			padding: 20px 0px 20px 10px;
			float: left;
		}
		#khuvuc{
			width: 250px;
			padding: 20px 0px 20px 10px;
			float: left;
		}
		#dinhmuc{
			width: 300px;
			padding: 20px 0px 20px 20px;
			float: left;
		}
		#tieuthu{
			width: 300px;
			padding: 20px 0px 20px 10px;
			float: left;
		}
		#thanhtien{
			width: 250px;
			padding: 20px 0px 20px 10px;
			float: left;
		}
		#p1, #p2, #p3, #p4, #p5, #p6{
			margin: 0px 0px;
			font-size: x-large;
			display: inline-block;
		}
		#i1, #i2, #i3, #i4{
			width: 130px;
			border-color: blue;
		}
		#i5{
			width: 200px;
			border-color: blue;
		}
	</style>
</head>
<body>
	<?php 
		$cu = $_GET['cu'];
		$moi = $_GET['moi'];
		$kv = $_GET['kv'];
		if ($cu >= $moi) {
			echo "Nhập sai chỉ số";
		}
	?>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">TÍNH TIỀN ĐIỆN</h4>
		</div>
		<form>
			<div id="cscu">
				<p id="p1">Chỉ số cũ</p> <br>
				<input id="i1" type="text" name="" value=<?php echo $cu; ?>>
			</div>
			<div id="csmoi">
				<p id="p2">Chỉ số mới</p> <br>
				<input id="i2" type="text" name="" value=<?php echo $moi; ?>>
			</div>
			<div id="khuvuc">
				<p id="p3">Khu vực</p>
				<select>
					<option value="1" <?php if(isset($_GET['kv']) && $kv == 1){
						echo "selected";
					} ?>>1</option>
					<option value="2" <?php if(isset($_GET['kv']) && $kv == 2){
						echo "selected";
					} ?>>2</option>
					<option value="3" <?php if(isset($_GET['kv']) && $kv == 3){
						echo "selected";
					} ?>>3</option>
				</select>
			</div>
			<div id="dinhmuc">
				<p id="p4">Định mức</p> <br>
				<input id="i3" type="text" name="" value="<?php 
					if($kv == 1){ $dm = 50; }
					elseif($kv == 2){ $dm = 100; }
					else{ $dm = 150; }
				echo $dm;?>">
			</div>
			<div id="tieuthu">
				<p id="p5">Tiêu thụ</p> <br>
				<input id="i4" type="text" name="" value="<?php echo $tt=$moi-$cu ?>">
			</div>
			<div id="thanhtien">
				<p id="p6">Thành Tiền</p> <br>
				<input id="i5" type="text" name="" value="<?php 
					if($tt <= $dm){
						$thanhtoan=$tt*450;
						echo "$tt*450=$thanhtoan";
					}
					else{
						$thanhtoan=$dm*450+($tt-$dm)*800;
						echo "$dm*450+($tt-$dm)*800=$thanhtoan";
					} 
				?>">
			</div>
		</form>
	</div>
</body>
</html>