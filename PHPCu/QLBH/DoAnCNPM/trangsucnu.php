<?php
session_start();
?>
<html>
<head>
<meta charset="UTF-8"/>
<style>
  *
 {
   margin:0;
   padding:0;
   font-family:Tahoma;
 }
  body
 {
   background: #f2f2f2;
   width: 100%;
   height: 1000px; 
 }
  #header
 { 
   background: #f2f2f2;
   height: 200px; 
   position:relative;   
 } 
  #header #container
 {
   background: #f2f2f2;
   height: 100px;
   width:80%;
   margin-top:1%;
   left:10%;
   position:absolute;
 }
  #header #container #logo
 {
   height:100px;
   width:10%;
   float:left;
 } 
  #header #container #logo img
 {
   height:100px;
   width:100%;
 } 
  #header #container #seach
 { background:#f2f2f2;
   height:100px;
   width:50%;
   float:left;
   margin-left:5%;
 } 
  #header #container #seach form
 { 
   line-height:100px;
 } 
  #header #container #seach form #tx
 { 
   font-size:30px;
   margin-left:90px;
 } 
  #header #container #seach form #sm
 { 
   font-size:20px;
 } 
  #header #container #login
 { background:#f2f2f2;
   height:100px;
   width:20%;
   float:left;
   margin-left:10%;
 } 
  #header #container #login a
 {
   color:#4d4d4d;
   text-decoration:none;
 }
  #header #container #login a #check
 {
   margin:-20px 0px 0px 45px;
 }
 #header #container #login #mid
 {
   margin:-20px 0px 0px 95px;
 }
 #header #container #login a #p1
 {
   margin:-19px 0px 0px 105px;
 }
  #header #container #login a:hover
 {
   color:red;
 }
  #header #container #login a img
 {
   width:13%;
   height:30px;
   margin:20px 0px 0px 10px;
 }
  #header #menu
 {
   background: #f2f2f2;
   height: 60px;
   width:80%;
   margin-top:9%;
   left:10%;
   position:absolute;
 }
  #header #menu ul
 {
   margin:0px;
   padding:0px;
 }
  #header #menu ul li
 {
   list-style:none;
   background: #f2f2f2;
   height: 60px;
   width:13%;
   line-height:60px;
   text-align:center;
   float:left;
   margin-left:0%;
   display:block;
 }
  #header #menu ul li a
 {
   display:block;
   text-decoration:none;
   color:#4d4d4d;
 }
  #header #menu ul li:hover 
 {
   background:pink;
 }
  #header #menu ul li:hover > ul
 {
   display:block;
 }
  #header #menu ul li ul
 {
   margin:0px;
   padding:0px;
   display:none;
 }
  #header #menu ul li ul li
 {
   list-style:none;
   background: #f2f2f2;
   height: 60px;
   width:100%;
   margin-left:0%;
   color:#c48c46;
 }
  #main
 {
   background: #f2f2f2;
   height: 500px;
 }	
  #main .list
 {
   background: #f2f2f2;
   height: 300px;
   width:19%;
   margin-top:7%;
   margin-left:10%;
   float:left;
 }  
  #main .list ul
 {
   margin:0px;
   padding:0px;
   
 }  
  #main .list ul li
 {
   list-style:none;
 }  
  #main .list ul li .price
 {
   color:red;
 }
  #main .list .see_move:hover
 {
   background:pink;
 }
  #main .list img
 {
   height: 300px;
   width:100%;
 }
  #footer
 {
   background: #f2f2f2;
   height: 300px;
 }  
  #footer p
 {
   margin-top:3%;
 }	 
</style>
</head>
<body>
  <div id="header">
    <div id="container">
	  <div id="logo">
	    <a href="login_ok.php">
	      <img src ="https://t4.ftcdn.net/jpg/03/31/84/47/240_F_331844776_sU4f3bWxu2zwKsqnNdJvjEcmlbj0hVSP.jpg" />
		</a>  
	  </div>
	  <div id="seach">
		<form method="POST" action="seach.php">
		  <input id="tx" type="text" name="tt" value=""/>
		  <input id="sm" type="submit" value="Seach" name="sm"/>
		</form>	
	  <?php
		if(isset($_POST['sm']))
		{
		  echo "
		<script>
                var src='".$_POST['tt']."';
                document.getElementById('tx').value=src;	      			  
        </script>
		";
		}
	  ?>
	  </div>
	  <div id="login">
	    <a href="rename.php" >
		  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/440px-User_icon_2.svg.png"/>
		  <?php
		    echo "<p id='check'>".$_SESSION['username']."</p>";
		  ?>
		</a>
		<p id="mid">/</p>
		<?php
		  echo "<a href='logout.php'><p id='p1'>Đăng xuất</p></a>";
		?>
	  </div>
	</div>
	<div id="menu">
	  <ul>
	    <li>
		  <a href="trangsucnam.php">Trang sức nam</a> 
		  <ul>
		    <li>
			  <a href="#">Dây da nam</a>
			</li>
			<li>
			  <a href="#">Nhẫn nam</a>
			</li>
		  </ul>
		</li>
		<li>
		  <a href="trangsucnu.php">Trang sức nữ</a> 
		  <ul>
		    <li>
			  <a href="#">Dây da nữ</a>
			</li>
			<li>
			  <a href="#">Nhẫn nữ</a>
			</li>
		  </ul>
		</li>
	  </ul>
	</div>
  </div>
  <div id="main">
<?php
  $dbServername="localhost";
  $dbUsername="root";
  $dbPassword="";
  $dbName="bantrangsuc";
  $conn= new mysqli($dbServername,$dbUsername, $dbPassword,$dbName);
  $query="select img  from trang_suc_nu";
  $rs= mysqli_query($conn,$query);
  $img=array();
  $count;
  if(mysqli_num_rows($rs)>0)
  {
    $i=0;
	while($row=mysqli_fetch_assoc($rs))
	{
	  $img[$i]= $row['img'];
	  $i++;
	  $count=$i;
    }
  }
  else
  {	 
    echo $rs;
  }	
  $query="select ten  from trang_suc_nu";
  $rs= mysqli_query($conn,$query);
  $ten=array();
  if(mysqli_num_rows($rs)>0)
  {
    $i=0;
	while($row=mysqli_fetch_assoc($rs))
	{
	  $ten[$i]= $row['ten'];
	  $i++;
    }
  }
  else
  {	 
    echo $rs;
  }	
  $query="select masp  from trang_suc_nu";
  $rs= mysqli_query($conn,$query);
  $masp=array();
  if(mysqli_num_rows($rs)>0)
  {
    $i=0;
	while($row=mysqli_fetch_assoc($rs))
	{
	  $masp[$i]= $row['masp'];
	  $i++;
    }
  }
  else
  {	 
    echo $rs;
  }	
  $query="select gia  from trang_suc_nu";
  $rs= mysqli_query($conn,$query);
  $gia=array();
  if(mysqli_num_rows($rs)>0)
  {
    $i=0;
	while($row=mysqli_fetch_assoc($rs))
	{
	  $gia[$i]= $row['gia'];
	  $i++;
    }
  }
  else
  {	 
    echo $rs;
  }	
  $query="select ma  from trang_suc_nu";
  $rs= mysqli_query($conn,$query);
  $ma=array();
  if(mysqli_num_rows($rs)>0)
  {
    $i=0;
	while($row=mysqli_fetch_assoc($rs))
	{
	  $ma[$i]= $row['ma'];
	  $i++;
    }
  }
  else
  {	 
    echo $rs;
  }
  $list=$count;
  $dem=0;
  while($list>0)
  {
	$list=$list-3;
	$dem++;
  }
  $dem=$dem*500;
  echo "
        <script>
        document.getElementById('main').style.height='".$dem."';
		</script>
  ";
  for($i=0;$i<$count;$i++)
  { 
    echo "
	      <div class='list'>
	        <ul>
	          <li>
		        <a href ='".$ma[$i].".php' >
				  <img src='".$img[$i]."'/>
				</a>
		        <p> ".$ten[$i]."</p>
		        <p>".$masp[$i]."</p>
		        <p class='price'>Giá: ".$gia[$i]."</p>
				<a class='see_move' href ='".$ma[$i].".php' >Xem chi tiết</a>
		      </li>
	        </ul>
	      </div>";
  }
?> 
</div>
  <div id="footer">
    <p>Đề Thi Lập Trình Web</p>
    <p>Sinh Viên Thực Hiện Trang Web</p>
    <p>Chịu trách nhiệm nội dung: Nguyễn Văn Hiệp</p>
    <p>D13CNPM7 MSV:18810310559 Thực Hiện Đề Tài Môn Học</p>
  </div>
</body>
</html>