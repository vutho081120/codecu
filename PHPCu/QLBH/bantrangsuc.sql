-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 03, 2021 lúc 04:09 PM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `bantrangsuc`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chi_tiet_hd`
--

CREATE TABLE `chi_tiet_hd` (
  `ma_hd` int(11) NOT NULL,
  `ma_sp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `chi_tiet_hd`
--

INSERT INTO `chi_tiet_hd` (`ma_hd`, `ma_sp`) VALUES
(90, '1000'),
(91, '1000'),
(93, '1000'),
(100, '1000'),
(101, '1000'),
(102, '1001');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hang_hoa`
--

CREATE TABLE `hang_hoa` (
  `ma_hd` int(11) NOT NULL,
  `ten` varchar(255) NOT NULL,
  `gia` varchar(255) NOT NULL,
  `sdt` varchar(50) NOT NULL,
  `ngay` datetime NOT NULL,
  `trang_thai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `hang_hoa`
--

INSERT INTO `hang_hoa` (`ma_hd`, `ten`, `gia`, `sdt`, `ngay`, `trang_thai`) VALUES
(100, 'Ghế trưởng phòng A146', '2080000 ', '0987654321', '2020-12-30 16:27:03', 1),
(101, 'Ghế trưởng phòng A146', '2080000 ', '0987654321', '2020-12-30 17:10:42', 1),
(102, 'Ghế trưởng phòng A430', '2607000\r\n ', '0987654321', '2021-01-05 09:25:42', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loai_sp`
--

CREATE TABLE `loai_sp` (
  `phan_loai` varchar(255) NOT NULL,
  `ten_loai` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `loai_sp`
--

INSERT INTO `loai_sp` (`phan_loai`, `ten_loai`) VALUES
('0', 'Nhẫn Vàng'),
('1', 'Bông Tai'),
('2', 'Nhẫn Bạc'),
('3', 'Dây Chuyền');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `sdt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `login`
--

INSERT INTO `login` (`id`, `user`, `pass`, `sdt`) VALUES
(1, 'admin', 'admin', '098765555'),
(118, 'hiep', '1', '0987654321'),
(120, 'tuan', '1123', '0393651809'),
(131, 'hiep01', '11', '032584853');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `san_pham`
--

CREATE TABLE `san_pham` (
  `id` int(11) NOT NULL,
  `anh` varchar(2550) NOT NULL,
  `ten` varchar(255) NOT NULL,
  `gia` varchar(255) NOT NULL,
  `ma_sp` varchar(255) NOT NULL,
  `phan_loai` int(50) NOT NULL,
  `mo_ta` varchar(5000) NOT NULL,
  `so_luong` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `san_pham`
--

INSERT INTO `san_pham` (`id`, `anh`, `ten`, `gia`, `ma_sp`, `phan_loai`, `mo_ta`, `so_luong`) VALUES
(1, 'http://www.bannoithat.com/upload/Product/250xNxaaa_1482306570.png.pagespeed.ic.Cfqfv0xhsN.webp', 'Ghế trưởng phòng A146', '2080000 ', '1000', 0, 'Mô tả<br> Nhờ có thiết kế đơn giản hóa nên mẫu ghế văn phòng tựa lưới sở hữu ưu điểm gọn nhẹ cùng chiều cao ghế vừa vặn với dáng người. Ghế xoay văn phòng mang phong cách hiện đại, trang nhã nên rất phù hợp với không gian văn phòng hiện đại ngày nay. Chất lượng sản phẩm cao cấp nhất thị trường với mức giá phẳng chăng, thu hút được đông đảo khách hàng yêu thích và lựa chọn.<br> Thông tin chi tiết về ghế xoay văn phòng tựa lưới Hi <br>  Chất liệu: Đệm mút bọc nỉ, tựa lưới nano chống thấm, chân, tay ghế bằng nhựa<br>  Kích thước (D x R x C): 650 x 500 x (920 x 990) mm<br>  Bán kính chân ghế: 325 mm<br>  Màu sắc: như sản phẩm mẫu hoặc khách hàng lựa chọn mẫu vải lưới cho sản phẩm<br>', 11),
(4, 'http://www.bannoithat.com/upload/Product/YC6204_1308909350.jpg', 'Tủ hồ sơ gỗ', '3980000', '3000', 2, 'Mô tả<br>\r\nTủ để đồ dùng gia đình phong cách hiện đại có chất lượng tuyệt vời và mức giá thành hợp lý đang là sản phẩm được chọn mua nhiều nhất thời gian gần đây. Tủ để đồ dùng gia đình gồm 6 ngăn tủ cung cấp không gian lưu trữ thoải mái cho người dùng sử dụng với nhiều mục đích đa dạng.\r\nThông tin chi tiết về tủ để đồ dùng gia đình phong cách hiện đại:<br> \r\nHướng dẫn sử dụng: tủ để đồ dùng phòng khách, phòng ngủ<br>\r\nChất liệu: gỗ sồi tự nhiên phun sơn S8 (chất lượng cao)<br>\r\n\r\nKích thước (D x R x C): 1200 x 400 x 780mm<br>\r\n\r\nMàu sắc: khách hàng tùy ý lựa chọn màu sơn tại cửa hàng<br>', 10),
(5, 'http://www.bannoithat.com/upload/Product/250xNxTu31_1285823379.jpg.pagespeed.ic.UlUBix1lyG.webp', 'Tủ hồ sơ 3 cánh', '3200000', '3001', 2, 'Mô tả<br>\r\nTủ để đồ dùng gia đình phong cách hiện đại có chất lượng tuyệt vời và mức giá thành hợp lý đang là sản phẩm được chọn mua nhiều nhất thời gian gần đây. Tủ để đồ dùng gia đình gồm 6 ngăn tủ cung cấp không gian lưu trữ thoải mái cho người dùng sử dụng với nhiều mục đích đa dạng.\r\nThông tin chi tiết về tủ để đồ dùng gia đình phong cách hiện đại:<br> \r\nHướng dẫn sử dụng: tủ để đồ dùng phòng khách, phòng ngủ<br>\r\nChất liệu: gỗ sồi tự nhiên phun sơn S8 (chất lượng cao)<br>\r\n\r\nKích thước (D x R x C): 1200 x 400 x 780mm<br>\r\n\r\nMàu sắc: khách hàng tùy ý lựa chọn màu sơn tại cửa hàng<br>', 10),
(7, 'http://www.bannoithat.com/upload/Product/A-430_1482306444.jpg', 'Ghế trưởng phòng A430', '2607000\r\n ', '1001', 0, 'Mô tả<br> Nhờ có thiết kế đơn giản hóa nên mẫu ghế văn phòng tựa lưới sở hữu ưu điểm gọn nhẹ cùng chiều cao ghế vừa vặn với dáng người. Ghế xoay văn phòng mang phong cách hiện đại, trang nhã nên rất phù hợp với không gian văn phòng hiện đại ngày nay. Chất lượng sản phẩm cao cấp nhất thị trường với mức giá phẳng chăng, thu hút được đông đảo khách hàng yêu thích và lựa chọn.<br> Thông tin chi tiết về ghế xoay văn phòng tựa lưới Hi <br>  Chất liệu: Đệm mút bọc nỉ, tựa lưới nano chống thấm, chân, tay ghế bằng nhựa<br>  Kích thước (D x R x C): 650 x 500 x (920 x 990) mm<br>  Bán kính chân ghế: 325 mm<br>  Màu sắc: như sản phẩm mẫu hoặc khách hàng lựa chọn mẫu vải lưới cho sản phẩm<br>', 10),
(8, 'http://www.bannoithat.com/upload/Product/A75_1482306326.png', 'Ghế trưởng phòng A76', '4818000 \r\n ', '1002', 0, 'Mô tả<br> Nhờ có thiết kế đơn giản hóa nên mẫu ghế văn phòng tựa lưới sở hữu ưu điểm gọn nhẹ cùng chiều cao ghế vừa vặn với dáng người. Ghế xoay văn phòng mang phong cách hiện đại, trang nhã nên rất phù hợp với không gian văn phòng hiện đại ngày nay. Chất lượng sản phẩm cao cấp nhất thị trường với mức giá phẳng chăng, thu hút được đông đảo khách hàng yêu thích và lựa chọn.<br> Thông tin chi tiết về ghế xoay văn phòng tựa lưới Hi <br>  Chất liệu: Đệm mút bọc nỉ, tựa lưới nano chống thấm, chân, tay ghế bằng nhựa<br>  Kích thước (D x R x C): 650 x 500 x (920 x 990) mm<br>  Bán kính chân ghế: 325 mm<br>  Màu sắc: như sản phẩm mẫu hoặc khách hàng lựa chọn mẫu vải lưới cho sản phẩm<br>', 10),
(9, 'http://www.bannoithat.com/upload/Product/Tu21_1285823496.jpg', 'Tủ hồ sơ 2 cánh', '2720000', '3003', 2, 'Mô tả<br>\r\nTủ để đồ dùng gia đình phong cách hiện đại có chất lượng tuyệt vời và mức giá thành hợp lý đang là sản phẩm được chọn mua nhiều nhất thời gian gần đây. Tủ để đồ dùng gia đình gồm 6 ngăn tủ cung cấp không gian lưu trữ thoải mái cho người dùng sử dụng với nhiều mục đích đa dạng.\r\nThông tin chi tiết về tủ để đồ dùng gia đình phong cách hiện đại:<br> \r\nHướng dẫn sử dụng: tủ để đồ dùng phòng khách, phòng ngủ<br>\r\nChất liệu: gỗ sồi tự nhiên phun sơn S8 (chất lượng cao)<br>\r\n\r\nKích thước (D x R x C): 1200 x 400 x 780mm<br>\r\n\r\nMàu sắc: khách hàng tùy ý lựa chọn màu sơn tại cửa hàng<br>', 10),
(10, 'http://www.bannoithat.com/upload/Product/250xNxA76,P20,282,29_1482306122.jpg.pagespeed.ic.t1ZO2KbfvX.webp', 'Ghế trưởng phòng A75', '5.086.000 đ\r\n ', '1003', 0, 'Mô tả<br> Nhờ có thiết kế đơn giản hóa nên mẫu ghế văn phòng tựa lưới sở hữu ưu điểm gọn nhẹ cùng chiều cao ghế vừa vặn với dáng người. Ghế xoay văn phòng mang phong cách hiện đại, trang nhã nên rất phù hợp với không gian văn phòng hiện đại ngày nay. Chất lượng sản phẩm cao cấp nhất thị trường với mức giá phẳng chăng, thu hút được đông đảo khách hàng yêu thích và lựa chọn.<br> Thông tin chi tiết về ghế xoay văn phòng tựa lưới Hi <br>  Chất liệu: Đệm mút bọc nỉ, tựa lưới nano chống thấm, chân, tay ghế bằng nhựa<br>  Kích thước (D x R x C): 650 x 500 x (920 x 990) mm<br>  Bán kính chân ghế: 325 mm<br>  Màu sắc: như sản phẩm mẫu hoặc khách hàng lựa chọn mẫu vải lưới cho sản phẩm<br>', 10);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `hang_hoa`
--
ALTER TABLE `hang_hoa`
  ADD PRIMARY KEY (`ma_hd`);

--
-- Chỉ mục cho bảng `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `san_pham`
--
ALTER TABLE `san_pham`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `hang_hoa`
--
ALTER TABLE `hang_hoa`
  MODIFY `ma_hd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT cho bảng `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT cho bảng `san_pham`
--
ALTER TABLE `san_pham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
