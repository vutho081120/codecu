<!DOCTYPE html>
<html>
<head>
	<title>Khai báo thông tin</title>
	<style type="text/css">
		#noidung{
			margin: 10px auto;
			width: 600px;
			min-height: 450px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 600px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: white;
			text-align: center;
			background-color: blue;
		}
		#trong{
			margin: 10px auto;
			width: 600px;
			min-height: 110px;
			position: relative;
		}
		#ht1{
			position: absolute;
			top: 0px;
			left: 30px;
			font-style: italic;
			font-weight: bold;
			font-size: 20px;
		}
		#ht2{
			position: absolute;
			top: 40px;
			left: 30px;
			color: red;
		}
	</style>
</head>
<body>
	<div id="trong">
		<?php
			$hten = "";
			$ten = "";
			$gt = "";
			$x4 = "";
			$flag1 = false;
			if (isset($_GET['Submit'])) {
				if (isset($_GET['sex']) && isset($_GET['ngonngu']) && isset($_GET['fname']) && isset($_GET['lname'])) {
					$ten = $_GET['lname'];
					$hten = $_GET['fname'];
					$gt = $_GET['sex'];
					$x4 = $_GET['ngonngu'];
					if (empty($_GET['fname']) || empty($_GET['lname'])) {
						$kq = "Mời bạn nhập đủ họ tên và tên";
					}
					else{
						$flag1 = true;
						$kq = "Tên: ".$ten;
						$kq = $kq."<br>Họ và đệm: ".$hten;
						$kq = $kq."<br>Giới tinh: ".$gt;
						$kq = $kq."<br>Ngoại ngữ: ".implode(", ", $x4);
					}
				}
				else{
					$kq = "Mời bạn nhập đủ thông tin";
				}
			}
			else{
				$kq = "";
			}
			if ($flag1 == true) {
				echo "<div id='ht1'>".$kq."</div>";
			}
			else{
				echo "<div id='ht2'>".$kq."</div>";
			}
		?>
	</div>
	<div id="noidung">
		<div id="tieude">
			<h2 style="display: inline-block;">FORM KHAI BÁO THÔNG TIN</h2>
		</div>
		<div id="form">
			<form action="khaibao.php" method="get">
			  	<font size="5">
			  		<div style="float: left; width: 250px; padding-left: 40px;">
			  			<br><b>First Name</b> <br><br>
			  			<b>Last Name</b> <br><br>
			  			<b>Sex</b> <br><br>
			  			<b>Languge</b>
			  		</div>
			  		<div style="float: left; width: 250px;">
			  			<br><input id="i1" type="text" name="fname" value="<?php echo $hten ?>" size="30"> <br><br>
			  			<input id="i2" type="text" name="lname" value="<?php echo $ten ?>" size="30"> <br><br>
		  				Male: <input id="i3" type="radio" name="sex"
		  				<?php if (isset($_GET['sex']) && $_GET['sex']=="Male") echo "checked";?> value="Male"> 
		  				Female: <input id="i4" type="radio" name="sex"
		  				<?php if (isset($_GET['sex']) && $_GET['sex']=="Female") echo "checked";?> value="Female"> <br><br>
			  			<div style="clear: left; width: 63px; float: left;">
				  			Pháp <br>
				  			Anh <br>
				  			Nhật <br>
				  			Hàn <br>
				  			Trung
			  			</div>
			  			<div>
					  		<input id="i5" type="checkbox" name="ngonngu[]" value="Pháp" 
					  		<?php 
					  			if(isset($_GET['ngonngu']) && in_array("Pháp", $_GET['ngonngu'])){
					  				echo "checked";
					  			}
					  		?>> <br>
					  		<input id="i6" type="checkbox" name="ngonngu[]" value="Anh" 
					  		<?php 
					  			if(isset($_GET['ngonngu']) && in_array("Anh", $_GET['ngonngu'])){
					  				echo "checked";
					  			}
					  		?>> <br>
					  		<input id="i7" type="checkbox" name="ngonngu[]" value="Nhật" 
					  		<?php 
					  			if(isset($_GET['ngonngu']) && in_array("Nhật", $_GET['ngonngu'])){
					  				echo "checked";
					  			}
					  		?>> <br>
					  		<input id="i8" type="checkbox" name="ngonngu[]" value="Hàn" 
					  		<?php 
					  			if(isset($_GET['ngonngu']) && in_array("Hàn", $_GET['ngonngu'])){
					  				echo "checked";
					  			}
					  		?>> <br>
					  		<input id="i9" type="checkbox" name="ngonngu[]" value="Trung" 
					  		<?php 
					  			if(isset($_GET['ngonngu']) && in_array("Trung", $_GET['ngonngu'])){
					  				echo "checked";
					  			}
					  		?>> <br>
				  		</div>
			  		</div>
			  		<div style="clear: left; padding-left: 40px;width: 505px;">
						<input type="submit" name="Submit" value="Submit" style="padding: 10px 20px; 
		  					border-radius: 10px; float: right; clear: right;">
					</div>
			  	</font>
			</form>	
			
			<button style="margin-left: 40px; padding: 10px 20px; border-radius: 10px;">
				<a href="khaibao.php" style="text-decoration: none; color: black;">Reset</a>
			</button>
		</div>
	</div>
</body>
</html>