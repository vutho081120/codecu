<!DOCTYPE html>
<html>
<head>
	<title>Vẽ hình</title>
	<style type="text/css">
		#noidung{
			margin: 10px auto;
			width: 600px;
			min-height: 450px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
			padding: 10px 10px;
		}
	</style>
</head>
<body>
	<font size="3">
		<div id="noidung">
			<b>Vẽ tam giác sao</b> <br>
			<?php 
				for($i = 0; $i <= 6; $i++) { 
					for($j = 0; $j < $i; $j++) { 
						echo "* ";
					}
					echo "<br>";
				}
			?>
			<b>Vẽ Tam Giác Sao</b> <br>
			<?php 
				for($i = 0; $i <= 5; $i++) { 
					for($j = 0; $j < $i; $j++) { 
						echo "* ";
					}
					echo "<br>";
				}
			?>
			<b>Vẽ Tam Giác Cân</b> <br>
			<?php 
				for($i = 0; $i <= 5; $i++) { 
					for($j = 0; $j <= 5-$i; $j++) { 
						echo "&nbsp"; 
					} 
					for($k = 0; $k <= $i; $k++) { 
						echo "*";
					} 
					echo "<br>"; 
				}
			?>
			<b>Vẽ Tam Giác Sao</b> <br>
			<?php 
				for($i = 0; $i <= 6; $i++) { 
					for($j = 0; $j < $i; $j++) { 
						echo "*";
					}
					echo "<br>";
				}
				for($i = 0; $i <= 5; $i++) { 
					for($j = 5; $j > $i; $j--) { 
						echo '*'; 
					}
					echo "<br>";
				}
			?>
			<b>Vẽ hình thoi</b> <br>
			<?php 
				for($i = 0; $i <= 5; $i++) { 
					for($j = 0; $j <= 5-$i; $j++) { 
						echo "&nbsp"; 
					} 
					for($k = 0; $k <= $i; $k++) { 
						echo "*";
					} 
					echo "<br>"; 
				}
				for($i = 0; $i <= 5; $i++) { 
					for($j = 0; $j <= $i+1; $j++) { 
						echo "&nbsp"; 
					} 
					for($j = 5; $j > $i; $j--) { 
						echo '*'; 
					} 
					echo "<br>"; 
				}
			?>
			<b>Vẽ hình thoi</b> <br>
			<?php 
				for($i = 0; $i <= 3; $i++) { 
					for($j = 0; $j <= $i; $j++) { 
						echo "&nbsp&nbsp"; 
					} 
					for($k = 0; $k <= 5; $k++) { 
						echo "*";
					} 
					echo "<br>"; 
				}
			?>
			<b>Vòng lặp for</b> <br>
			<?php 
				for ($i = 1; $i <= 5; $i++){
				    for ($j = 1; $j <= 6; $j++){
				      	if ($i == 1 || $j == 1 || $i == 5 || $j == 6){
				        	echo "*";
				      	}
				        else{
				        	echo "&nbsp&nbsp";
				        }
				    }
					echo "<br>";
				}
			?>
		</div>
	</font>
</body>
</html>