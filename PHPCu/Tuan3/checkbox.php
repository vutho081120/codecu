<!DOCTYPE html>
<html>
<head>
	<title>Khai báo thông tin</title>
	<style type="text/css">
		#noidung{
			margin: 100px auto;
			width: 600px;
			min-height: 450px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 600px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: white;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<?php 
			$x4 = "";
			if (isset($_GET['submit'])) {
				if (isset($_GET['ngonngu'])) {
					$x4 = $_GET['ngonngu'];
				}
			}
		?>
		<div id="tieude">
			<h2 style="display: inline-block;">FORM KHAI BÁO THÔNG TIN</h2>
		</div>
		<div id="form">
			<form action="checkbox.php" method="get">
			  	<font size="5">
			  		<div style="float: left; width: 250px;">
			  			<div style="clear: left; width: 63px; float: left;">
				  			Pháp <br>
				  			Anh <br>
				  			Nhật <br>
				  			Hàn <br>
				  			Trung
			  			</div>
			  			<div>
					  		<input id="i5" type="checkbox" name="ngonngu[]" value="Pháp" 
					  		<?php 
					  			if(isset($_GET['ngonngu']) && in_array("Pháp", $x4)){
					  				echo "checked";
					  			}
					  		?>> <br>
					  		<input id="i6" type="checkbox" name="ngonngu[]" value="Anh" 
					  		<?php 
					  			if(isset($_GET['ngonngu']) && in_array("Anh", $x4)){
					  				echo "checked";
					  			}
					  		?>> <br>
					  		<input id="i7" type="checkbox" name="ngonngu[]" value="Nhật" 
					  		<?php 
					  			if(isset($_GET['ngonngu']) && in_array("Nhật", $x4)){
					  				echo "checked";
					  			}
					  		?>> <br>
					  		<input id="i8" type="checkbox" name="ngonngu[]" value="Hàn" 
					  		<?php 
					  			if(isset($_GET['ngonngu']) && in_array("Hàn", $x4)){
					  				echo "checked";
					  			}
					  		?>> <br>
					  		<input id="i9" type="checkbox" name="ngonngu[]" value="Trung" 
					  		<?php 
					  			if(isset($_GET['ngonngu']) && in_array("Trung", $x4)){
					  				echo "checked";
					  			}
					  		?>> <br>
				  		</div>
			  		</div>
				  	<div style="clear: left; padding-left: 40px;width: 505px;">
				  		<input type="submit" name="" value="Reset" style="padding: 10px 20px; 
				  		border-radius: 10px;">
				  		<!-- su dung button va the a de link toi trang ban dau -->
				  		<input type="submit" name="submit" value="Submit" style="padding: 10px 20px; 
				  		border-radius: 10px; float: right; clear: right;">
				  	</div>
			  	</font>
			</form>
		</div>
	</div>
</body>
</html>