<!DOCTYPE html>
<html>
<head>
	<title>Phương trình bậc hai</title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: black;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<?php 
		$a = $_GET['a'];
		$b = $_GET['b'];
		$c = $_GET['c'];
		if (!isset($a) || !isset($b)  || !isset($c)) {
			$kq = "Mời bạn nhập đủ dữ liệu";
		}
		elseif (!is_numeric($a) || !is_numeric($b) || !is_numeric($c)) {
			$kq = "Dữ liệu nhập số sai kiểu";
		}
		else{
			if($a == 0){
		        if($b == 0){
		                if($c == 0){
		                    $kq = "Phương trình đã cho có vô số nghiệm";
		                }
		                else{
		                    $kq = "Phương trình đã cho vô nghiệm";;
		                }
		            }
		        else{
		            $kq = "Phương trình đã cho có nghiệm duy nhất <br> x = ".(-$c/$b);
		        }
		    }
		    else{
		    	$delta = $b*$b - 4*$a*$c;
			    if($delta < 0){
			    	$kq = "Phương trình đã cho vô nghiệm";
			    }
			    else if($delta == 0){
			    	$kq = "Phương trình đã cho có nghiệm kép <br> x1 = x2 = ".(-$b/(2*$a));
			    }
			    else{
			        $x1 = (-$b + sqrt($delta)) / (2*$a);
			        $x2 = (-$b - sqrt($delta)) / (2*$a);
			        $kq = "Phương trình đã cho có hai nghiệm phân biệt <br> x1 = ".$x1."<br> x2 = ".$x2;
			    }
		    }		
		}
	?>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Giải phương trình bậc hai</h4>
		</div>
		<div id="form" style="padding-left: 50px;">
			<font size="6">
	  			<?php 
					echo $kq;
	  			?>
			</font>
		</div>
	</div>
</body>
</html>