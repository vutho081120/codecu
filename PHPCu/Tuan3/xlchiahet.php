<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Chia hết cho 2 và tận cùng không là 4</h4>
		</div>
		<div>
			<?php
				$so1 = $_GET['so1'];
				$so2 = $_GET['so2'];
				$j = 0;
				$kq = "";
				$flag = false;
				if(empty($so1) || empty($so2)){
					$kq = "Mời bạn nhập đầy đủ dữ liệu";
				}
				elseif (!is_numeric($so1) || !is_numeric($so2)) {
					$kq = "Dữ liệu nhập số sai kiểu";
				}
				else{
					$flag = true;
					if ($so1 > $so2 ) {
						$kq = "Số đầu nhỏ hơn số cuối";
						$flag = false;
					}
					else{
						for($i = $so1; $i <= $so2; $i++){
							if (($i % 2) == 0) {
								if(($i % 10) != 4){
									$j = $j + 1;
									if($j >= 2){
										$kq = $kq.", ".$i;
									}
									else{
										$kq = $kq.$i;
									}
								}
							}
						}
					}		
				}
				if ($flag == true) {
					echo "Dãy số nhập vào là: <br>";
					echo $kq;
				}
				else{
					echo $kq;
				}	
			?>
		</div>
	</div>
</body>
</html>