<!DOCTYPE html>
<html lang="vi">
<head>
	<title>Quản lý tài khoản</title>
	<meta charset="utf-8"/>
	<style type="text/css">
		#noidung{
			margin: 50px auto;
			width: 1000px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 1000px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: white;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<?php 
    $conn=mysqli_connect('localhost','root','','qlbh') or die("Không thể kết nối tới cơ sở dữ liệu");
        if($conn){mysqli_query($conn,"SET NAMES 'utf8'");
        }else{
            echo "Bạn đã kết nối thất bại".mysqli_connect_erro();
        }
        $sql="select * from tai_khoan";
        $do = mysqli_query($conn, $sql);
?>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Quản lý tài khoản</h4>
		</div>
		<div>
			<table border="1px">
				<?php if(mysqli_num_rows($do) > 0) {?>
				<tr>
					<td>Tên đăng nhập</td>
					<td>Email</td>
					<td>Mật khẩu</td>
					<td>Số điện thoại</td>
					<td>Quyền</td>
					<td colspan ="2">Thao tác</td>
				</tr>
				<?php 
    				 while($row=mysqli_fetch_array($do)){?>
    				 	<tr>
	    					<td><?php echo $row[0]; ?></td>
	    					<td><?php echo $row[1]; ?></td>
	    					<td><?php echo $row[2]; ?></td>
	    					<td><?php echo $row[3]; ?></td>
	    					<td><?php echo $row[4]; ?></td>
	    					<td><button><a href="suauser.php">Sửa</a></button></td>
	    					<td><button><a href="xoauser">Xóa</a></button></td>
	    				</tr>
	    			<?php } ?>
			</table>
			<?php } ?>
			<button>
				<a href="themuser.php">Thêm tài khoản</a>
			</button>
			<br> <br>
			<button>
				<a href="admin.php">Về trang admin</a>
			</button>
		</div>
	</div>
</body>
</html>