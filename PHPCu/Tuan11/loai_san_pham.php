<!DOCTYPE html>
<html lang="vi">
<head>
	<title>Kết nối</title>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="style.css">
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: white;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<?php 
    $conn=mysqli_connect('localhost','root','','ql_shop_quan_ao') or die("Không thể kết nối tới cơ sở dữ liệu");
        if($conn){mysqli_query($conn,"SET NAMES 'utf8'");
        }else{
            echo "Bạn đã kết nối thất bại".mysqli_connect_erro();
        }
        $sql="select ma_loai, ten_loai_san_pham, noi_san_xuat from loai_san_pham";
        $do = mysqli_query($conn, $sql);
?>
<body>
    <div id="noidung">
    	<?php 	if(mysqli_num_rows($do) > 0) {?>
    			<table>
    				<caption><h2>Thông tin loại sản phẩm</h2></caption>
	    				<tr>
	    					<td>Mã loại</td>
	    					<td>Tên loại</td>
	    					<td>Nơi sản xuất</td>
	    				</tr>
    				<?php 
    				 while($row=mysqli_fetch_array($do)){?>
    				 	<tr>
	    					<td><?php echo $row[0]; ?></td>
	    					<td><?php echo $row[1]; ?></td>
	    					<td><?php echo $row[2]; ?></td>
	    				</tr>
	    			<?php } ?>
    			</table>
    	 <?php } ?>
    </div>
</body>
</html>