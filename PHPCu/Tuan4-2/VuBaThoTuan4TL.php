<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Mảng</h4>
		</div>
		<br>
		<div>
			<font size="5" color="red">
				<p>1.Khai báo và hiển thị</p>
			</font>
			<?php 
				$ds = array(
					"Khuất Vân Lan" => array(
						"NS" => "4/5/1989",
						"Quê quán" => "Hà Nam"
					),
					"Hồ Đức Minh" => array(
						"NS" => "3/3/1990",
						"Quê quán" => "Ninh Bình"
					),
					"Vũ Lan Ngọc" => array(
						"NS" => "20/8/1991",
						"Quê quán" => "Thái Bình"
					),
					"Nguyễn Anh Quân" => array(
						"NS" => "2/6/1990",
						"Quê quán" => "Cao Bằng"
					),
					"Trần Văn Thịnh" => array(
						"NS" => "4/8/1992",
						"Quê quán" => "Hà Giang"
					),
					"Nguyễn Ngọc Hoài" => array(
						"NS" => "14/2/1992",
						"Quê quán" => "Lạng Sơn"
					),
					"Nguyễn Thu Lan" => array(
						"NS" => "27/10/1991",
						"Quê quán" => "Hải Dương"
					),
					"Phạm thị Mai" => array(
						"NS" => "5/5/1992",
						"Quê quán" => "Hải Phòng"
					),
					"Kiều Đức Mạnh" => array(
						"NS" => "23/9/1990",
						"Quê quán" => "Yên Bái"
					),
					"Lê Kiều Trinh" => array(
						"NS" => "4/1/1993",
						"Quê quán" => "Lào Cai"
					)
				);
				echo "<pre>";
				print_r($ds);
				echo "</pre>";
			?>
			<font size="5" color="red">
				<p>2.Trích xuất mảng họ tên</p>
			</font>
			<?php
				$hoten = array_keys($ds);
				echo "<pre>";
				print_r($hoten);
				echo "</pre>";
			?>
			<font size="5" color="red">
				<p>3.Trích xuất mảng thông tin</p>
			</font>
			<?php 
				$thongtin = array_values($ds);
				echo "<pre>";
				print_r($thongtin);
				echo "</pre>";
			?>
			<font size="5" color="red">
				<p>4.Hiển thị danh sách sinh viên</p>
			</font>
			<font size="5">
				<b>DANH SÁCH SINH VIÊN:</b>
			</font>
			<div style="margin-top: 20px; margin-bottom: 10px;">
				<table border="6" align="center">
					<tr height="20" style="border: 1px solid; text-align: center;">
	                   	<td width="50"><b>STT</b></td>
	                    <td width="150"><b>Họ và tên</b></td>
	                    <td width="150"><b>Ngày Sinh</b></td>
	                    <td width="150"><b>Quê Quán</b></td>
	              	</tr>
	               	<?php
	               		$stt = 0;
	               		for ($i=0; $i <= 9; $i++) { 
	               			$stt = $i + 1;
	               			echo "
	               			<tr height=6>
			                    <td>$stt</td>
			                    <td><b>$hoten[$i]</b></td>
			                    <td align=center>".$thongtin[$i]["NS"]."</td>
			                    <td align=center>".$thongtin[$i]["Quê quán"]."</td>
	               			</tr>
	               			";
	               		}
	                ?>
				</table>
			</div>
		</div>
	</div>
</body>
</html>