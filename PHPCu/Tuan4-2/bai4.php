<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Mảng</h4>
		</div>
		<br>
		<div>
			<?php 
				$mang = array(
					"Xử lý ảnh" => "Đặng Thành Trung",
					"Ngôn ngữ hình thức" => "Nguyễn Thị Hồng",
					"Lập trình hướng đối tượng" => "Nguyễn Thị Hạnh",
					"Giải tích" => "Nguyễn Vân Anh",
					"Phương pháp nghiên cứu khoa học" => "Nguyễn Thị Kim Ngân",
					"Trí tuệ nhân tạo" => "Hồ Cẩm Hà"
				);
				foreach ($mang as $x => $y) {
					echo "Môn $x do giảng viên $y phụ trách. <br>";
				}
			?>
		</div>
	</div>
</body>
</html>