<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Ước chung lớn nhất</h4>
		</div>
		<?php 
			function UCLN($a, $b)
			{
				if($a == $b){
					return $a;
				}
				elseif($a > $b){
					return UCLN($a-$b,$b);
				}
				else{
					return UCLN($a,$b-$a);
				}
			}
			function UC($a, $n)
			{
				if($n == 1){
					return $a[0];
				}
				else{
					return UCLN($a[$n-1],UC($a,$n-1));
				}
			}
			if (isset($_GET['Submit'])) {
				$chuoi = $_GET['chuoi'];
			}
			else {
				$chuoi = "";
			}
			$kq = "";
			$flag1 = false;
			if (empty($chuoi)) {
				$kq = "Mời bạn nhập dữ liệu";
			}
			else {
				$flag1 = true;
				$mang = explode(" ", $chuoi);
				$kq = UC($mang, count($mang));
			}
		?>
		<form action="test.php" method="get">
			<br>Nhập chuỗi các số cần tìm (cách nhau bởi dấu cách) 
			<br> <input type="text" name="chuoi" value="<?php echo $chuoi ?>" size="50">
			<br><br><input type="Submit" name="Submit" value="Tìm"><br><br>
			<?php
				if ($flag1 == false) {
					echo $kq;
				}
				else{
					echo "Ước chung lớn nhất của dãy số là: $kq";
				}
			?>
		</form>
	</div>
</body>
</html>