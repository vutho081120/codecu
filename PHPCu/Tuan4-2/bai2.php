<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Vòng lặp</h4>
		</div>
		<br>
		<form>
			<table border="1" align="center">
				<tr height="6px" style="border: 1px solid;">
                    <td width="110">STT</td>
                    <td width="110">Họ và tên</td>
                    <td width="110">Đề tài</td>
                    <td width="110">Xếp hạng</td>
              	</tr>
               	<?php
               		for ($i=1; $i <= 10; $i++) { 
               			echo "
               			<tr height=6>
	                    <td>$i</td>
	                    <td>Sinh viên $i</td>
	                    <td>Đề tài $i</td>
	                    <td>Xếp hạng $i</td>
               			</tr>
               			";
               		}
                ?>
			</table>
		</form>
	</div>
</body>
</html>