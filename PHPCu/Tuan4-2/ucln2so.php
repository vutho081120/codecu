<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Ước chung lớn nhất của hai số</h4>
		</div>
		<?php 
			function UCLN($a, $b)
			{
				if($a == $b){
					return $a;
				}
				elseif($a > $b){
					return UCLN($a-$b,$b);
				}
				else{
					return UCLN($a,$b-$a);
				}
			}
			if (isset($_GET['Submit'])) {
				$a = $_GET['so1'];
				$b = $_GET['so2'];
			}
			else {
				$a = "";
				$b = "";
			}
			$kq = "";
			$flag1 = false;
			if (empty($a) || empty($b)) {
				$kq = "Mời bạn nhập dữ liệu";
			}
			elseif (!is_numeric($a) || !is_numeric($b)) {
				$kq = "Dữ liệu nhập số sai kiểu";
			}
			else {
				$flag1 = true;
				$kq = UCLN($a,$b);
			}
		?>
		<form action="VuBaThoTuan4VN.php" method="get">
			<br>Số thứ nhất <input type="text" name="so1" value="" size="10">
			Số thứ hai <input type="text" name="so2" value="" size="10">
			<input type="Submit" name="Submit" value="Tính"> <br><br>
			<?php
				if ($flag1 == false) {
					echo $kq;
				}
				else{
					echo "Ước chung lớn nhất của $a và $b là: $kq";
				}
			?>
		</form>
	</div>
</body>
</html>