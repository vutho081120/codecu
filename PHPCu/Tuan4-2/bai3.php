<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Vòng lặp</h4>
		</div>
		<br>
		<form>
			<table border="1" align="center">
               	<?php
               		for ($i=1; $i <= 9; $i++) {
               			echo "
               				<tr height=6px>
               			";
               			for ($j=1; $j <= 9; $j++) {
               				$kq = $i*$j;
               				echo "
		                    <td>$j*$i=$kq</td>
               				";
               			}
               			echo "</tr>";
               		}
                ?>
			</table>
		</form>
	</div>
</body>
</html>