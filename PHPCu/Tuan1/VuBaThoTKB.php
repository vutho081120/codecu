<!DOCTYPE html>
<html>
<head>
	<title>Thời Khóa Biểu</title>
	<style type="text/css">
		tr {
			text-align: center;
		}
	</style>
</head>
<body>
	<div align="center">
		<table border="1">
			<tr height="50px">
				<td width="100px">Tiết</td>
				<td width="100px">T2</td>
				<td width="100px">T3</td>
				<td width="100px">T4</td>
				<td width="100px">T5</td>
				<td width="100px">T6</td>
				<td width="100px">T7</td>
				<td width="100px">CN</td>
			</tr>
			<tr height="50px">
				<td width="100px">1</td>
				<td width="100px" rowspan="3">Mạng máy tính nâng cao</td>
				<td width="100px" rowspan="3">Thực hành kỹ thuật máy tính và mạng</td>
				<td width="100px">Nghỉ</td>
				<td width="100px" rowspan="3">Lập trình mạng 2</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
			</tr>
			<tr height="50px">
				<td width="100px">2</td>
				<td width="100px">Nghỉ</td>
				<td width="100px" rowspan="4">Phân tích thiết kế hệ thống</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
			</tr>
			<tr height="50px">
				<td width="100px">3</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
			</tr>
			<tr height="50px">
				<td width="100px">4</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
			</tr>
			<tr height="50px">
				<td width="100px">5</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
			</tr>
			<tr height="50px">
				<td width="100px">6</td>
				<td width="100px" rowspan="2">Phân tích thiết kế thuật toán</td>
				<td width="100px" rowspan="3">Trí tuệ nhân tạo</td>
				<td width="100px" rowspan="2">Bài tập lớn</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
			</tr>
			<tr height="50px">
				<td width="100px">7</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
			</tr>
			<tr height="50px">
				<td width="100px">8</td>
				<td width="100px">Nghỉ</td>
				<td width="100px" rowspan="3">Tiếng anh chuyên ngành 2</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
			</tr>
			<tr height="50px">
				<td width="100px">9</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
			</tr>
			<tr height="50px">
				<td width="100px">10</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
				<td width="100px">Nghỉ</td>
			</tr>
		</table>
	</div>
	<div align="center" style="margin-top: 10px;">
		<?php 
		$thu = "T5";
		$tiet = 3;
		switch ($thu) {
			case "T2":
				if($tiet == 1 || $tiet == 2 || $tiet == 3){
					$kq = "Thứ hai tiết ".$tiet.":Môn học (Mạng máy tính nâng cao)";
				}
				elseif ($tiet == 6 || $tiet == 7) {
					$kq = "Thứ hai tiết ".$tiet.":Môn học (Phân tích thiết kế thuật toán)";
				}
				else{
					$kq = "Thứ hai tiết ".$tiet.":Môn học (Nghỉ)";
				}
				break;
			case "T3":
				if($tiet == 1 || $tiet == 2 || $tiet == 3){
					$kq = "Thứ ba tiết ".$tiet.":Môn học (Thực hành kỹ thuật máy tính và mạng)";
				}
				elseif ($tiet == 6 || $tiet == 7 || $tiet == 8) {
					$kq = "Thứ ba tiết ".$tiet.":Môn học (Trí tuệ nhân tạo)";
				}
				else{
					$kq = "Thứ ba tiết ".$tiet.":Môn học (Nghỉ)";
				}
				break;
			case "T4":
				if($tiet == 6 || $tiet == 7){
					$kq = "Thứ tư tiết ".$tiet.":Môn học (Bài tập lớn)";
				}
				elseif ($tiet == 8 || $tiet == 9 || $tiet == 10) {
					$kq = "Thứ tư tiết ".$tiet.":Môn học (Tiếng anh chuyên ngành 2)";
				}
				else{
					$kq = "Thứ tư tiết ".$tiet.":Môn học (Nghỉ)";
				}
				break;
			case "T5":
				if($tiet == 1 || $tiet == 2 || $tiet == 3){
					$kq = "Thứ năm tiết ".$tiet.":Môn học (Lập trình mạng 2)";
				}
				else{
					$kq = "Thứ năm tiết ".$tiet.":Môn học (Nghỉ)";
				}
				break;
			case "T6":
				if($tiet == 2 || $tiet == 3 || $tiet == 4 || $tiet == 5){
					$kq = "Thứ sáu tiết ".$tiet.":Môn học (Phân tích thiết kế hệ thống)";
				}
				else{
					$kq = "Thứ sáu tiết ".$tiet.":Môn học (Nghỉ)";
				}
				break;
			case "T7":
				$kq = "Thứ bảy tiết ".$tiet.":Môn học (Nghỉ)";
				break;
			case "CN":
				$kq = "Chủ nhật tiết ".$tiet.":Môn học (Nghỉ)";
				break;
			default:
				$kq = "Thứ ... tiết ...:Môn học (Nghỉ)";
				break;
		}
		echo $kq;
	?>
	</div>
</body>
</html>