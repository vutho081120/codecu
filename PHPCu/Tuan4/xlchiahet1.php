<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Chia hết</h4>
		</div>
		<div>
			<?php
				function Chia_het_n($x, $y, $n)
				{
					$kq = "";
					$j = 0;
					for($i = $x; $i <= $y; $i++){
						if (($i % $n) == 0) {
							$j = $j + 1;
							if($j >= 2){
								$kq = $kq.", ".$i;
							}
							else{
								$kq = $kq.$i;
							}
						}
					}
					return $kq;
				}
				$so1 = $_GET['so1'];
				$so2 = $_GET['so2'];
				$ch = $_GET["ch"];
				$kq = "";
				$flag1 = false;
				if(empty($so1) || empty($so2)){
					$kq = "Mời bạn nhập đầy đủ dữ liệu";
				}
				elseif (!is_numeric($so1) || !is_numeric($so2)) {
					$kq = "Dữ liệu nhập số sai kiểu";
				}
				else{
					$flag1 = true;
					if ($so1 > $so2 ) {
						$kq = "Số đầu nhỏ hơn số cuối";
						$flag1 = false;
					}
					else{
						$flag2 = true;
						$kq = Chia_het_n($so1, $so2, $ch);
					}
				}
				if ($flag1 == true && $flag2 == true) {
					echo "Dãy số chia hết cho ".$ch." là: <br>";
					echo $kq;
				}
				else{
					echo $kq;
				}	
			?>
		</div>
		<div style="padding-top: 70px; padding-bottom: 5px;">
			<button style="margin-left: 20px; padding: 10px 20px; border-radius: 10px;">
				<a href="chiahet1.php" style="text-decoration: none; color: black;">Quay trở lại</a>
			</button>
		</div>
	</div>
</body>
</html>