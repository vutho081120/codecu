<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Ví dụ</h4>
		</div>
		<?php 
			function giaithua($x){
				if ($x > 0) {
					return $x * giaithua($x-1);
				}
				else{
					return 1;
				}
			}
		?>
		<div>
			Hàm <br>
			100! = <?php echo giaithua(100); ?>
		</div>
	</div>
</body>
</html>