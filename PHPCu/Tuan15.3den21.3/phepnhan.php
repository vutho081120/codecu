<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Phép nhân</h4>
		</div>
		<?php
			function UCLN($a, $b)
			{
				if($a == $b){
					return $a;
				}
				elseif($a > $b){
					return UCLN($a-$b,$b);
				}
				else{
					return UCLN($a,$b-$a);
				}
			}
			function rut_gon_ps($tu, $mau){
				$kq = "";
				$UCLN = UCLN($tu, $mau);
				$tu = $tu / $UCLN;
				$mau = $mau / $UCLN;
				$kq = $tu."/".$mau;
				return $kq;
			}
			function KTPS($x)
			{
				if (is_numeric($x)) {
					return false;
				}
				else{
					$PS = explode("/", $x);
					$tu = $PS[0];
					$mau = $PS[1];
					if (count($PS) > 2 || !is_numeric($tu) || !is_numeric($mau)){
						return false;
					}
					else{
						return true;
					}
				}	
			}
			if (isset($_GET['Submit'])) {
				$so1 = $_GET['so1'];
				$so2 = $_GET['so2'];
				if (empty($so1) || empty($so2)) {
					$kq = "Mời bạn nhập dữ liệu";
				}
				else{
					if (KTPS($so1) == true && KTPS($so2) == true) {
						$PS1 = explode("/", $so1);
						$tu1 = $PS1[0];
						$mau1 = $PS1[1];
						$PS2 = explode("/", $so2);
						$tu2 = $PS2[0];
						$mau2 = $PS2[1];
						$tu3 = (float)$tu1 * (float)$tu2;
						$mau3 = (float)$mau1 * (float)$mau2;
						$kq = $tu3."/".$mau3;
						//$kq = rut_gon_ps($tu3, $mau3);
					}
					elseif (KTPS($so1) == true && is_numeric($so2) == true) {
						$PS1 = explode("/", $so1);
						$tu1 = $PS1[0];
						$mau1 = $PS1[1];
						$tu3 = (float)$tu1 * (float)$so2;
						$mau3 = (float)$mau1;
						$kq = $tu3."/".$mau3;
						//$kq = rut_gon_ps($tu3, $mau3);
					}
					elseif (is_numeric($so1) == true && KTPS($so2) == true) {
						$PS2 = explode("/", $so2);
						$tu2 = $PS2[0];
						$mau2 = $PS2[1];
						$tu3 = (float)$tu2 * (float)$so1;
						$mau3 = (float)$mau2;
						$kq = $tu3."/".$mau3;
						//$kq = rut_gon_ps($tu3, $mau3);
					}
					elseif (is_numeric($so1) == true && is_numeric($so2) == true) {
						$kq = (float)$so1 * (float)$so2;
					}
					else{
						$kq = "Dữ liệu nhập sai kiểu";
					}
				}
			}
			else{
				$kq = "";
				$so1 = "";
				$so2 = "";
			}
		?>
		<div>
			<form action="phepnhan.php" method="get">
				Số thứ nhất <input type="text" name="so1" value="<?php echo $so1 ?>"> <br>
				Số thứ hai <input type="text" name="so2" value="<?php echo $so2 ?>"> <br>
				<input type="Submit" name="Submit" value="Nhân"> <br>
				<?php
					echo "Kết quả là: ".$kq;
				?>
			</form>
		</div>
	</div>
</body>
</html>