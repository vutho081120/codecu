<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Phân số</h4>
		</div>
		<?php
			function UCLN($a, $b)
			{
				if($a == $b){
					return $a;
				}
				elseif($a > $b){
					return UCLN($a-$b,$b);
				}
				else{
					return UCLN($a,$b-$a);
				}
			}
			function rut_gon_ps($tu, $mau){
				$kq = "";
				$UCLN = UCLN($tu, $mau);
				$tu = $tu / $UCLN;
				$mau = $mau / $UCLN;
				$kq = $tu."/".$mau;
				return $kq;
			}
			if (isset($_GET['Submit'])) {
				$phanso = $_GET['phanso'];
				$PS = explode("/", $phanso);
				$tu = $PS[0];
				$mau = $PS[1];
				if (count($PS) > 2 || !is_numeric($tu) || !is_numeric($mau)){
					$kq = "Dữ liệu sai kiểu";
				}
				else{
					$kq = rut_gon_ps($tu, $mau);
				}
			}
			else{
				$kq = "";
				$phanso = "";
			}
		?>
		<div>
			<form action="formphanso.php" method="get">
				Phân số <input type="text" name="phanso" value="<?php echo $phanso ?>"> <br>
				<input type="Submit" name="Submit" value="Tìm phân số tối giản"> <br>
				<?php
					echo $kq;
				?>
			</form>
		</div>
	</div>
</body>
</html>