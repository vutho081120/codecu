<?php 
	SESSION_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;"></h4>
		</div>
		<div>
			<?php 
				if(isset($_SESSION["user"]) && isset($_SESSION["mk"])){
					echo "Tên đăng nhập là ".$_SESSION["user"];
					echo "Mật khẩu là ".$_SESSION["mk"];
				}
				else{
					echo "Không tồn tại SESSION";
				}
			?>
		</div>
	</div>
</body>
</html>