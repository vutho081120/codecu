<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Ví dụ</h4>
		</div>
		<?php 
			function tongsotn($x){
				$kq = 0;
				$x = abs($x);
				while ($x != 0) {
					$kq = $kq + $x % 10;
					$x = $x / 10;
				}
				return $kq;
			}
		?>
		<div>
			Hàm <br>
			<?php echo tongsotn(-1236); ?>
		</div>
	</div>
</body>
</html>