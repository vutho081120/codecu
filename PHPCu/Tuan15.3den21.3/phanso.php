<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: red;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Phân số</h4>
		</div>
		<?php
			function UCLN($a, $b)
			{
				if($a == $b){
					return $a;
				}
				elseif($a > $b){
					return UCLN($a-$b,$b);
				}
				else{
					return UCLN($a,$b-$a);
				}
			}
			function rut_gon_ps($x){
				$PS = explode("/", $x);
				$tu = $PS[0];
				$mau = $PS[1];
				$UCLN = UCLN($tu, $mau);
				$tu = $tu / $UCLN;
				$mau = $mau / $UCLN;
				return $tu."/".$mau;
			}
		?>
		<div>
			Bài toán phân số <br>
			Phân số: 15/20 <br>
			Phân số tối giản: <?php echo rut_gon_ps("15/20") ?>
		</div>
	</div>
</body>
</html>