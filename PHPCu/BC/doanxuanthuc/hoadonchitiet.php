<!DOCTYPE html>
<html lang="vi">
<?php 
	session_start();
?>
<head>
	<meta charset="utf-8">
	<title>Hóa đơn chi tiết</title>
	<style>
		<?php 
			include "styleSanpham.css";
		?>
	</style>
</head>
<?php 
    $conn=mysqli_connect('localhost','root','','doanxuanthuc') or die("Không thể kết nối tới cơ sở dữ liệu");
        if($conn){mysqli_query($conn,"SET NAMES 'utf8'");
        }else{
            echo "Bạn đã kết nối thất bại".mysqli_connect_erro();
        }
        $sql="select * from hoa_don_chi_tiet";
        $do = mysqli_query($conn, $sql);
?>
<body>
	<div id="menu_top">
	  	<ul class="nav">
			<li><a href="admin.php">Trang chủ</a></li>
			<li><a href="loaisanpham.php">Loại sản phẩm</a><li>
			<li><a href="sanpham.php">Sản phẩm</a></li>
			<li><a href="nguoidung.php">Người dùng</a></li>
			<li><a href="hoadon.php">Hóa đơn</a></li>
			<li><a href="hoadonchitiet.php" class="active">Hóa đơn chi tiết</a></li>
			<li><a href="dangxuat.php">Đăng xuất</a></li>
		</ul>
	</div>
	<div id="banner">
	</div>
  	<div id="main">
	    <div id="left">
	    	<div id="tieude">
	     		<h3 style="background: #6699CC; height: 40px; margin-top: 0px; 
	     		line-height: 40px;">Danh sách hóa đơn chi tiết</h3>
	    	</div>
	    	<div id="noidung">
	    		<table border="1px">
					<?php if(mysqli_num_rows($do) > 0) {?>
					<tr>
						<td>Mã hóa đơn chi tiết</td>
						<td>Mã hóa đơn</td>
						<td>Mã sản phẩm</td>
						<td>Giá sản phẩm</td>
						<td>Số lượng</td>
						<td colspan ="2">Thao tác</td>
					</tr>
					<?php 
	    				 while($row=mysqli_fetch_array($do)){?>
	    				 	<tr>
		    					<td><?php echo $row[0]; ?></td>
		    					<td><?php echo $row[1]; ?></td>
		    					<td><?php echo $row[2]; ?></td>
		    					<td><?php echo $row[3]; ?></td>
		    					<td><?php echo $row[4]; ?></td>
		    					<td><button><a href="suahoadonchitiet.php">Sửa</a></button></td>
		    					<td><button><a href="xoahoadonchitiet.php">Xóa</a></button></td>
		    				</tr>
		    			<?php } ?>
				</table>
				<?php } ?>
	    	</div>
	  	</div>
	</div>
	<div id="footer"> 
		<font size="3">
			<p>Địa chỉ: Khoa Công nghệ thông tin, 136 Xuân Thủy</p>
			<p>website: fit.hnue.edu.vn</p>
		</font>
	</div>
</body>
</html>