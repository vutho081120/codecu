<!DOCTYPE html>
<html lang="vi">
<head>
	<title>Xóa hóa đơn chi tiết</title>
	<meta charset="utf-8"/>
	<style type="text/css">
		#noidung{
			margin: 50px auto;
			width: 1000px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 1000px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: white;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<?php
		 $conn=mysqli_connect('localhost','root','','doanxuanthuc') or die("Không thể kết nối tới cơ sở dữ liệu");
		        if($conn){mysqli_query($conn,"SET NAMES 'utf8'");
		        }else{
		            echo "Bạn đã kết nối thất bại".mysqli_connect_erro();
		        }
	?>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Xóa hóa đơn chi tiết</h4>
		</div>
		<div>
			<form action="xoahoadonchitiet.php" method="post">
				<table border="1px">
					<tr>
						<td>Mã hóa đơn chi tiết</td>
						<td>
							<select name="mahoadonchitiet">
								<?php 
								$sql_mahoadonchitiet = "select * from hoa_don_chi_tiet";
  								$do = mysqli_query($conn, $sql_mahoadonchitiet);
								if(mysqli_num_rows($do) > 0) {
									while($row=mysqli_fetch_array($do)){?>
	    				 				<option value="<?php echo $row['0']; ?>"><?php echo $row[0]; ?></option>
		    						<?php } ?>
		    					<?php } ?>
							</select>
						</td>
					</tr>
				</table>
				<br>
				<input type="Submit" name="S_xoa" value="Xóa">
			</form>
			<br> <br>
			<button>
				<a href="hoadonchitiet.php">Về trang hóa đơn chi tiết</a>
			</button>
		</div>
	</div>
	<?php
	        if (isset($_POST["S_xoa"])) {
	        	
	        	$mahoadonchitiet = $_POST["mahoadonchitiet"];
	  			
	        	$sql_xoa = "delete from hoa_don_chi_tiet where ma_hoa_don_chi_tiet = '$mahoadonchitiet'";
	        	
	        	if (mysqli_query($conn, $sql_xoa)) {
	        		header("location: hoadonchitiet.php");
	        	}
		        else{
		        	echo "Xóa không thành công".mysqli_error($conn);
		        }
		        mysqli_close($conn);
	        }
	?>
</body>
</html>