<!DOCTYPE html>
<html lang="vi">
<?php 
	session_start();
?>
<head>
	<meta charset="utf-8">
	<title>Admin</title>
	<style>
		<?php 
			include "styleAdmin.css";
		?>
	</style>
</head>
<body>
	<div id="menu_top">
	  	<ul class="nav">
			<li><a href="admin.php" class="active">Trang chủ</a></li>
			<li><a href="loaisanpham.php">Loại sản phẩm</a></li>
			<li><a href="sanpham.php">Sản phẩm</a></li>
			<li><a href="nguoidung.php">Người dùng</a></li>
			<li><a href="hoadon.php">Hóa đơn</a></li>
			<li><a href="hoadonchitiet.php">Hóa đơn chi tiết</a></li>
			<li><a href="dangxuat.php">Đăng xuất</a></li>
		</ul>
	</div>

	<div id="banner">
		<div style="text-align: center;">
			<h1 style="margin: 0px 0px;">Welcome <?php echo $_SESSION['ho_ten']; ?></h1>
		</div>
	</div>

  	<div id="main">
	    <div id="left">
	    	<div id="tieude">
	     		<h3 style="background: #6699CC; height: 40px; margin-top: 0px; 
	     		line-height: 40px;">NỘI DUNG</h3>
	    	</div>
	    	<div id="noidung">
	    		
	    	</div>
	  	</div>
		<div id="right">
		    <div class="menu_right">
		      <h3>Danh mục tin tức</h3>
		      <ul>
		        <li><a href="">Khuyến mại</a></li>
		        <li><a href="">Sản phẩm bán chạy</a></li>
		        <li><a href="">Quà tặng</a></li>
		        <li><a href="">Liên hệ Facebook</a></li>
		      </ul>
		    </div>
		</div>
	</div>
	<div id="footer"> 
		<font size="3">
			<p>Địa chỉ: Khoa Công nghệ thông tin, 136 Xuân Thủy</p>
			<p>website: fit.hnue.edu.vn</p>
		</font>
	</div>
</body>
</html>