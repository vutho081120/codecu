<!DOCTYPE html>
<html lang="vi">
<?php 
	session_start();
?>
<head>
	<meta charset="utf-8">
	<title>Loại sản phẩm</title>
	<style>
		<?php 
			include "styleSanpham.css";
		?>
	</style>
</head>
<?php 
    $conn=mysqli_connect('localhost','root','','doanxuanthuc') or die("Không thể kết nối tới cơ sở dữ liệu");
        if($conn){mysqli_query($conn,"SET NAMES 'utf8'");
        }else{
            echo "Bạn đã kết nối thất bại".mysqli_connect_erro();
        }
        $sql="select * from loai_san_pham";
        $do = mysqli_query($conn, $sql);
?>
<body>
	<div id="menu_top">
	  	<ul class="nav">
			<li><a href="admin.php">Trang chủ</a></li>
			<li><a href="loaisanpham.php" class="active">Loại sản phẩm</a><li>
			<li><a href="sanpham.php">Sản phẩm</a></li>
			<li><a href="nguoidung.php">Người dùng</a></li>
			<li><a href="hoadon.php">Hóa đơn</a></li>
			<li><a href="hoadonchitiet.php">Hóa đơn chi tiết</a></li>
			<li><a href="dangxuat.php">Đăng xuất</a></li>
		</ul>
	</div>
	<div id="banner">
	</div>
  	<div id="main">
	    <div id="left">
	    	<div id="tieude">
	     		<h3 style="background: #6699CC; height: 40px; margin-top: 0px; 
	     		line-height: 40px;">Danh sách loại sản phẩm</h3>
	    	</div>
	    	<div id="noidung">
	    		<table border="1px">
					<?php if(mysqli_num_rows($do) > 0) {?>
					<tr>
						<td>Mã loại sản phẩm</td>
						<td>Tên loại sản phẩm</td>
						<td>Mô tả</td>
						<td colspan ="2">Thao tác</td>
					</tr>
					<?php 
	    				 while($row=mysqli_fetch_array($do)){?>
	    				 	<tr>
		    					<td><?php echo $row[0]; ?></td>
		    					<td><?php echo $row[1]; ?></td>
		    					<td><?php echo $row[2]; ?></td>
		    					<td><button><a href="sualoaisanpham.php">Sửa</a></button></td>
		    					<td><button><a href="xoaloaisanpham.php">Xóa</a></button></td>
		    				</tr>
		    			<?php } ?>
				</table>
				<?php } ?>
			<button>
				<a href="themloaisanpham.php">Thêm loại sản phẩm</a>
			</button>
	    	</div>
	  	</div>
	</div>
	<div id="footer"> 
		<font size="3">
			<p>Địa chỉ: Khoa Công nghệ thông tin, 136 Xuân Thủy</p>
			<p>website: fit.hnue.edu.vn</p>
		</font>
	</div>
</body>
</html>