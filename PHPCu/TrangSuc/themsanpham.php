<!DOCTYPE html>
<html lang="vi">
<head>
	<title>Thêm sản phẩm</title>
	<meta charset="utf-8"/>
	<style type="text/css">
		#noidung{
			margin: 50px auto;
			width: 1000px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 1000px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: white;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<?php
    $conn=mysqli_connect('localhost','root','','doanxuanthuc') or die("Không thể kết nối tới cơ sở dữ liệu");
        if($conn){mysqli_query($conn,"SET NAMES 'utf8'");
        }else{
            echo "Bạn đã kết nối thất bại".mysqli_connect_erro();
        }
?>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Thêm loại sản phẩm</h4>
		</div>
		<div>
			<form action="themsanpham.php" method="post">
				<table border="1px">
					<tr>
						<td>Mã loại sản phẩm</td>
						<td>
							<select name="maloai">
								<?php 
								$sql_maloai = "select * from loai_san_pham";
  								$do = mysqli_query($conn, $sql_maloai);
								if(mysqli_num_rows($do) > 0) {
									while($row=mysqli_fetch_array($do)){?>
	    				 				<option value="<?php echo $row['0']; ?>"><?php echo $row[1]; ?></option>
		    						<?php } ?>
		    					<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Tên sản phẩm</td>
						<td><input type="text" name="txt_tensanpham" size="50"></td>
					</tr>
					<tr>
						<td>Ảnh sản phẩm</td>
						<td><input type="text" name="txt_anhsanpham" size="50"></td>
					</tr>
					<tr>
						<td>Nhà sản xuất</td>
						<td><input type="text" name="txt_nhasanxuat" size="50"></td>
					</tr>
					<tr>
						<td>Mô tả</td>
						<td><input type="text" name="txt_mota" size="50"></td>
					</tr>
					<tr>
						<td>Giá sản phẩm</td>
						<td><input type="text" name="txt_giasanpham" size="50"></td>
					</tr>
					<tr>
						<td>Số lượng</td>
						<td><input type="text" name="txt_soluong" size="50"></td>
					</tr>
					<tr>
						<td>Ngày nhập</td>
						<td>
							<input type="datetime-local" name="ngaynhap">
						</td>
					</tr>
				</table>
				<br>
				<input type="Submit" name="S_them" value="Thêm">
			</form>
			<br> <br>
			<button>
				<a href="sanpham.php">Về trang sản phẩm</a>
			</button>
		</div>
	</div>
	<?php 
		if (isset($_POST["S_them"])) {
        	
        	$maloai = $_POST["maloai"];
        	$tensanpham = $_POST["txt_tensanpham"];
        	$anhsanpham = $_POST["txt_anhsanpham"];
        	$nhasanxuat = $_POST["txt_nhasanxuat"];
  			$mota = $_POST["txt_mota"];
  			$giasanpham = $_POST["txt_giasanpham"];
  			$soluong = $_POST["txt_soluong"];
  			$ngaynhap = $_POST["ngaynhap"];

        	$sql_them = "insert into san_pham (ma_loai_san_pham, ten_san_pham, anh_san_pham,
        	nha_san_xuat, mo_ta, gia_san_pham, so_luong, ngay_nhap) values ('$maloai', '$tensanpham',
        	'$anhsanpham', '$nhasanxuat', '$mota', '$giasanpham', '$soluong', '$ngaynhap')";
        	if (mysqli_query($conn, $sql_them)) {
        		header("location: sanpham.php");
        	}
	        else{
	        	echo "Thêm không thành công".mysqli_error($conn);
	        }
	        mysqli_close($conn);
        }
	?>
</body>
</html>