<!DOCTYPE html>
<html lang="vi">
<head>
	<title>Xóa người dùng</title>
	<meta charset="utf-8"/>
	<style type="text/css">
		#noidung{
			margin: 50px auto;
			width: 1000px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 1000px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: white;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<?php
		 $conn=mysqli_connect('localhost','root','','doanxuanthuc') or die("Không thể kết nối tới cơ sở dữ liệu");
		        if($conn){mysqli_query($conn,"SET NAMES 'utf8'");
		        }else{
		            echo "Bạn đã kết nối thất bại".mysqli_connect_erro();
		        }
	?>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">Xóa người dùng</h4>
		</div>
		<div>
			<form action="xoanguoidung.php" method="post">
				<table border="1px">
					<tr>
						<td>Mã người dùng</td>
						<td>
							<select name="manguoidung">
								<?php 
								$sql_manguoidung = "select * from nguoi_dung";
  								$do = mysqli_query($conn, $sql_manguoidung);
								if(mysqli_num_rows($do) > 0) {
									while($row=mysqli_fetch_array($do)){?>
	    				 				<option value="<?php echo $row['0']; ?>"><?php echo $row[0]; ?></option>
		    						<?php } ?>
		    					<?php } ?>
							</select>
						</td>
					</tr>
				</table>
				<br>
				<input type="Submit" name="S_xoa" value="Xóa">
			</form>
			<br> <br>
			<button>
				<a href="nguoidung.php">Về trang người dùng</a>
			</button>
		</div>
	</div>
	<?php
	        if (isset($_POST["S_xoa"])) {
	        	
	        	$manguoidung = $_POST["manguoidung"];
	  			
	        	$sql_xoa1 = "delete from hoa_don_chi_tiet where ma_hoa_don in (select ma_hoa_don 
	        	from hoa_don where ma_nguoi_dung = '$manguoidung')";
	        	$sql_xoa2 = "delete from hoa_don where ma_nguoi_dung = '$manguoidung'";
	        	$sql_xoa3 = "delete from nguoi_dung where ma_nguoi_dung = '$manguoidung'";
	        	if (mysqli_query($conn, $sql_xoa1) && mysqli_query($conn, $sql_xoa2)
	        		&& mysqli_query($conn, $sql_xoa3)) {
	        		header("location: nguoidung.php");
	        	}
		        else{
		        	echo "Xóa không thành công".mysqli_error($conn);
		        }
		        mysqli_close($conn);
	        }
	?>
</body>
</html>