<!DOCTYPE html>
<html lang="vi">
<?php 
	session_start();
?>
<head>
	<meta charset="utf-8">
	<title>Hóa đơn</title>
	<style>
		<?php 
			include "styleSanpham.css";
		?>
	</style>
</head>
<?php 
    $conn=mysqli_connect('localhost','root','','doanxuanthuc') or die("Không thể kết nối tới cơ sở dữ liệu");
        if($conn){mysqli_query($conn,"SET NAMES 'utf8'");
        }else{
            echo "Bạn đã kết nối thất bại".mysqli_connect_erro();
        }
        $sql="select * from hoa_don";
        $do = mysqli_query($conn, $sql);
?>
<body>
	<div id="menu_top">
	  	<ul class="nav">
			<li><a href="admin.php">Trang chủ</a></li>
			<li><a href="loaisanpham.php">Loại sản phẩm</a><li>
			<li><a href="sanpham.php">Sản phẩm</a></li>
			<li><a href="nguoidung.php">Người dùng</a></li>
			<li><a href="hoadon.php" class="active">Hóa đơn</a></li>
			<li><a href="hoadonchitiet.php">Hóa đơn chi tiết</a></li>
			<li><a href="dangxuat.php">Đăng xuất</a></li>
		</ul>
	</div>
	<div id="banner">
	</div>
  	<div id="main">
	    <div id="left">
	    	<div id="tieude">
	     		<h3 style="background: #6699CC; height: 40px; margin-top: 0px; 
	     		line-height: 40px;">Danh sách hóa đơn</h3>
	    	</div>
	    	<div id="noidung">
	    		<table border="1px">
					<?php if(mysqli_num_rows($do) > 0) {?>
					<tr>
						<td>Mã hóa đơn</td>
						<td>Mã người dùng</td>
						<td>Ngày mua</td>
						<td>Tổng tiền</td>
						<td colspan ="2">Thao tác</td>
					</tr>
					<?php 
	    				 while($row=mysqli_fetch_array($do)){?>
	    				 	<tr>
		    					<td><?php echo $row[0]; ?></td>
		    					<td><?php echo $row[1]; ?></td>
		    					<td><?php echo $row[2]; ?></td>
		    					<td><?php echo $row[3]; ?></td>
		    					<td><button><a href="suahoadon.php">Sửa</a></button></td>
		    					<td><button><a href="xoahoadon.php">Xóa</a></button></td>
		    				</tr>
		    			<?php } ?>
				</table>
				<?php } ?>
	    	</div>
	  	</div>
	</div>
	<div id="footer"> 
		<font size="3">
			<p>Địa chỉ: Khoa Công nghệ thông tin, 136 Xuân Thủy</p>
			<p>website: fit.hnue.edu.vn</p>
		</font>
	</div>
</body>
</html>