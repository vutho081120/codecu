<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: white;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">TRANG CHỦ</h4>
		</div>
		<div>
			<form method="get">
				<a href="thongtincanhan.php">Thông tin cá nhân</a> <br><br>
				<a href="thoikhoabieu.php">Thời khóa biểu</a> <br><br>
				<a href="dangkymonhoc.php">Đăng ký môn học</a> <br><br>
				<input type="submit" name="dangxuat" value="Đăng xuất">
				<?php 
					if (isset($_GET["dangxuat"])) {
						header("location: dangxuat.php");
					}
				?>
			</form>
		</div>
	</div>
</body>
</html>