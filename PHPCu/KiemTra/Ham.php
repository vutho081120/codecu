<?php 
	function ptb1($so1, $so2)
	{
		if($so1 == 0) {
				if ($so2 == 0) {
					$kq = "Phương trình vô số nghiệm";
				}
				else{
					$kq = "Phương trình vô nghiệm";
				}
			}
			else{
				$kq = "Phương trình có nghiệm X = ".(-$so2/$so1);
			}
		return $kq;
	}

	function ptb2($a, $b, $c)
	{
		if($a == 0){
	        if($b == 0){
                if($c == 0){
                    $kq = "Phương trình đã cho có vô số nghiệm";
                }
                else{
                    $kq = "Phương trình đã cho vô nghiệm";;
                }
            }
	        else{
	            $kq = "Phương trình đã cho có nghiệm duy nhất <br> x = ".(-$c/$b);
	        }
	    }
	    else{
	    	$delta = $b*$b - 4*$a*$c;
		    if($delta < 0){
		    	$kq = "Phương trình đã cho vô nghiệm";
		    }
		    else if($delta == 0){
		    	$kq = "Phương trình đã cho có nghiệm kép <br> x1 = x2 = ".(-$b/(2*$a));
		    }
		    else{
		        $x1 = (-$b + sqrt($delta)) / (2*$a);
		        $x2 = (-$b - sqrt($delta)) / (2*$a);
		        $kq = "Phương trình đã cho có hai nghiệm phân biệt <br> x1 = ".$x1."<br> x2 = ".$x2;
		    }
	    }
	    return $kq;
	}

	function tinh_toan($sothunhat, $sothuhai, $toantu)
	{
	    if ($toantu == '+') {
	        $kq = $sothunhat + $sothuhai;
	    }
	    else if ($toantu == '-') {
	        $kq = $sothunhat - $sothuhai;
	    }
	    else if ($toantu == '/') {
	        $kq = $sothunhat / $sothuhai;
	    }
	    else if ($toantu == '*') {
	        $kq = $sothunhat * $sothuhai;
	    }
	    else {
	       	$kq = 'Canh bao: Toan tu ban nhap vao khong chinh xac !!!';
	    }
	    return $kq;
	}

	function KTCH($x, $ch)
	{
		if ($x % $ch == 0) {
			return true;
		}
		else{
			return false;
		}
	}

	function KTTC($x, $tc){
		if ($x % 10 != $tc) {
			return true;
		}
		else{
			return false;
		}
	}

	function Chia_het_n_tancung($x, $y, $n, $tc)
	{
		$kq = "";
		$j = 0;
		for($i = $x; $i <= $y; $i++){
			if (($i % $n) == 0 && KTTC($i, $tc) == true) {
				$j = $j + 1;
				if($j >= 2){
					$kq = $kq.", ".$i;
				}
				else{
					$kq = $kq.$i;
				}
			}
		}
		return $kq;
	}

	function Chia_het_n_va($x, $y, $ch1, $ch2)
	{
		$kq = "";
		$j = 0;
		for($i = $x; $i <= $y; $i++){
			if (KTCH($i, $ch1) == true && KTCH($i, $ch2) == true) {
				$j = $j + 1;
				if($j >= 2){
					$kq = $kq.", ".$i;
				}
				else{
					$kq = $kq.$i;
				}
			}
		}
		return $kq;
	}

	function Chia_het_n_hoac($x, $y, $ch1, $ch2)
	{
		$kq = "";
		$j = 0;
		for($i = $x; $i <= $y; $i++){
			if (KTCH($i, $ch1) == true || KTCH($i, $ch2) == true) {
				$j = $j + 1;
				if($j >= 2){
					$kq = $kq.", ".$i;
				}
				else{
					$kq = $kq.$i;
				}
			}
		}
		return $kq;
	}

	function Tong_chia_het_n($x, $y, $n)
	{
		$t = 0;
		for($i = $x; $i <= $y; $i++){
			if (($i % $n) == 0) {
				$t = $t + $i;
			}
		}
		return $t;
	}

	function KTNC($nc){
		$kq = "";
		if ($nc >= 24) {
			$kq = 300000;
		}
		return $kq;
	}
	function KTGT($gt){
		$kq = "";
		$x = strtoupper($gt);
		if ($x == "FEMALE") {
			$kq = 200000;
		}
		else{
			$kq = "";
		}
		return $kq;
	}

	function UCLN($a, $b)
	{
		if($a == $b){
			return $a;
		}
		elseif($a > $b){
			return UCLN($a-$b,$b);
		}
		else{
			return UCLN($a,$b-$a);
		}
	}
	function UC($a, $n)
	{
		if($n == 1){
			return $a[0];
		}
		else{
			return UCLN($a[$n-1],UC($a,$n-1));
		}
	}
	function tim_uoc($x){
		$U[] = "";
		for($i = 1; $i <= $x; $i++){
			if($x % $i == 0){
				$U[] = $i;
			}
		}
		return $U;
	}
	function rut_gon_ps($tu, $mau){
		$kq = "";
		$UCLN = UCLN($tu, $mau);
		$tu = $tu / $UCLN;
		$mau = $mau / $UCLN;
		$kq = $tu."/".$mau;
		return $kq;
	}
	function KTPS($x)
	{
		if (is_numeric($x)) {
			return false;
		}
		else{
			$PS = explode("/", $x);
			$tu = $PS[0];
			$mau = $PS[1];
			if (count($PS) > 2 || !is_numeric($tu) || !is_numeric($mau)){
				return false;
			}
			else{
				return true;
			}
		}	
	}
	function nhan_2_ps($PS1, $PS2)
	{
		if (KTPS($so1) == true && KTPS($so2) == true) {
			$PS1 = explode("/", $so1);
			$tu1 = $PS1[0];
			$mau1 = $PS1[1];
			$PS2 = explode("/", $so2);
			$tu2 = $PS2[0];
			$mau2 = $PS2[1];
			$tu3 = (float)$tu1 * (float)$tu2;
			$mau3 = (float)$mau1 * (float)$mau2;
			$kq = $tu3."/".$mau3;
			//$kq = rut_gon_ps($tu3, $mau3);
		}
		elseif (KTPS($so1) == true && is_numeric($so2) == true) {
			$PS1 = explode("/", $so1);
			$tu1 = $PS1[0];
			$mau1 = $PS1[1];
			$tu3 = (float)$tu1 * (float)$so2;
			$mau3 = (float)$mau1;
			$kq = $tu3."/".$mau3;
			//$kq = rut_gon_ps($tu3, $mau3);
		}
		elseif (is_numeric($so1) == true && KTPS($so2) == true) {
			$PS2 = explode("/", $so2);
			$tu2 = $PS2[0];
			$mau2 = $PS2[1];
			$tu3 = (float)$tu2 * (float)$so1;
			$mau3 = (float)$mau2;
			$kq = $tu3."/".$mau3;
			//$kq = rut_gon_ps($tu3, $mau3);
		}
		elseif (is_numeric($so1) == true && is_numeric($so2) == true) {
			$kq = (float)$so1 * (float)$so2;
		}
		else{
			$kq = "Dữ liệu nhập sai kiểu";
		}
		return $kq;
	}
	function giaithua($x){
		if ($x > 0) {
			return $x * giaithua($x-1);
		}
		else{
			return 1;
		}
	}
	function tongsotn($x){
		$kq = 0;
		$x = abs($x);
		while ($x != 0) {
			$kq = $kq + $x % 10;
			$x = $x / 10;
		}
		return $kq;
	}
?>