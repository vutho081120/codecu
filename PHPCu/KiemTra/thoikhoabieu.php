<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#noidung{
			margin: 150px auto;
			width: 900px;
			min-height: 200px;
			border-style: solid;
			border-width: 3px;
			border-color: blue;
			border-radius: 10px;
		}
		#tieude{
			width: 900px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
			height: 60px;
			color: white;
			text-align: center;
			background-color: blue;
		}
	</style>
</head>
<body>
	<div id="noidung">
		<div id="tieude">
			<h4 style="display: inline-block;">THỜI KHÓA BIỂU</h4>
		</div>
		<div>
			<table border="1" align="center">
				<tr height="50px">
					<td width="200px">Môn học</td>
					<td width="200px">Thứ</td>
					<td width="200px">Tiết</td>
				</tr>
				<tr height="50px">
					<td width="200px">Mạng máy tính</td>
					<td width="200px">Thứ 2</td>
					<td width="200px">1-3</td>
				</tr>
				<tr height="50px">
					<td width="200px">Mạng máy tính</td>
					<td width="200px">Thứ 2</td>
					<td width="200px">6-8</td>
				</tr>
				<tr height="50px">
					<td width="200px">Lập trình mạng 2</td>
					<td width="200px">Thứ 4</td>
					<td width="200px">2-4</td>
				</tr>
				<tr height="50px">
					<td width="200px">Lập trình mạng 2</td>
					<td width="200px">Thứ 5</td>
					<td width="200px">1-3</td>
				</tr>
				<tr height="50px">
					<td width="200px">Thực hành mạng</td>
					<td width="200px">Thứ 3</td>
					<td width="200px">1-3</td>
				</tr>
				<tr height="50px">
					<td width="200px">Thực hành mạng</td>
					<td width="200px">Thứ 6</td>
					<td width="200px">6-8</td>
				</tr>
			</table>
			<button style="padding: 10px 20px; border-radius: 10px;">
				<a href="home.php" style="text-decoration: none; color: black;">Quay lại</a>
			</button>
		</div>
	</div>
</body>
</html>