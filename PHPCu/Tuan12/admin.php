<!DOCTYPE html>
<html lang="vi">
<?php 
	session_start();
	if (isset($_SESSION['user'])) {
		header("location: dangnhap.php");
	}
?>
<head>
	<meta charset="utf-8">
	<title>Admin</title>
	<link rel="stylesheet" href="styleHome.css">
</head>
<body>
	<div id="menu_top">
	  	<ul class="nav">
			<li><a href="admin.php" class="active">Trang chủ</a></li>
			<li>
			    <a href="">Sản phẩm</a>
			    <ul class="sub-menu">
			      	<li><a href="">Trang sức nam</a></li>
			      	<li>
				        <a href="">Trang sức nữ</a>
				        <ul class="sub-menu">
				            <li><a href="">Nhẫn</a></li>
				            <li><a href="">Vòng</a></li>
				            <li><a href="">Lắc</a></li>
				    	</ul>
			      	</li>
			      	<li><a href="">Vàng miếng</a></li>
			    </ul>
			</li>
			<li><a href="">Hàng mới</a></li>
			<li><a href="">Giỏ hàng</a></li>
			<li><a href="dangxuat.php">Đăng xuất</a></li>
		</ul>
	</div>

	<div id="banner">
		<div style="text-align: center;">
			<p style="display: inline-block; font-size: large;">Tìm kiếm sản phẩm theo giá</p>
			<select style="">
				<option>Chính thức</option>
				<option>Khuyến mãi</option>
			</select>
		</div>
	</div>

  	<div id="main">
	    <div id="left">
	    	<div id="tieude">
	    		<h1 style="margin: 0px 0px;">Welcome Admin</h1>
	     		<h3 style="background: #6699CC; height: 40px; margin-top: 0px; 
	     		line-height: 40px;">NỘI DUNG</h3>
	    	</div>
	    	<div id="noidung">
	    		
	    	</div>
	  	</div>
		<div id="right">
		    <div class="menu_right">
		      <h3>Danh mục tin tức</h3>
		      <ul>
		        <li><a href="">Khuyến mại</a></li>
		        <li><a href="">Sản phẩm bán chạy</a></li>
		        <li><a href="">Quà tặng</a></li>
		        <li><a href="">Liên hệ Facebook</a></li>
		      </ul>
		    </div>
		</div>
	</div>
	<div id="footer"> 
		<font size="3">
			<p>Địa chỉ: Khoa Công nghệ thông tin, 136 Xuân Thủy</p>
			<p>website: fit.hnue.edu.vn</p>
		</font>
	</div>
</body>
</html>